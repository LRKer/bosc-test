# Implementation Notes

## Blobs

In order to maximize the possibilities for manipulating the content of a PE file, processing is done with chunks of bytes called "blobs."  These are implemented by the `bosc::Blob` struct.  An implementation of the `MZ` file format using `bosc::Blob` can be found in the `bosc::MZ` struct.

Essentially we have a collection of bytes, and we can access them and manipulate them using the known offsets within PE file structures.  This allows moving items around in the file in arbitrary ways.

The interface is complicated by the fact that the PE file format (and its predecessors) use *little endian* byte layout exclusively, even on big-endian platforms.  To make this library work across these platforms, the little endian nature of byte storage is "baked into" the `bosc::Blob` struct.

The PE file format is implemented in the `bosc::pe` module.

## COFF Headers

The COFF headers are moderately complex beasts, and there is also a lot of repetetion in them.  For this reason, the `src/pe/coff_headers.rs` file is actually *generated* by a Python script.  See `etc/coff_headers_gen.py`.  This file is not exepected to change often, but *you should never modify* the Rust file.  Instead, modify the Python generator file.

```bash
$ python3 etc/coff_headers_gen.py > src/pe/coff_headers.py
```

The output of this will not look identical to what is already there, since the code in the Rust file is formatted (using `cargo fmt`) and has its header comment updated.

