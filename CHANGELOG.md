# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased] - Release Date

* Major cleanup of documentation and build process to try to make it more friendly.
* Lots of tests added.
* Added coverage tracking to the pipeline.

[Unreleased]: https://gitlab.com/sprowell/bosc/-/compare/mz-complete...master