# Roadmap for 1.2.0

* `[ ]` Provide a Python API

# Roadmap for 1.1.0

* `[ ]` Implement JSON (de)serialization
* `[ ]` Large-scale "round-trip" testing with a library of PE files

# Roadmap for 1.0.0

* `[x]` Replace existing system with the "blobbified" version
* `[ ]` Complete the initial PE implementation
  * `[ ]` Handle data directories
  * `[ ]` Handle the section table
  * `[ ]` Handle additional data
  * `[ ]` Parse
  * `[ ]` Generate
  * `[ ]` API examples and documentation
  * `[ ]` Doc tests and unit tests

# Wish List

This is a parking lot for things that "would be nice to have."

* `[ ]` Support the NE format
