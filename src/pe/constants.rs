// The Bosc Library
// Copyright (c) 2022 by Stacy Prowell.  All rights reserved.
// https://gitlab.com/sprowell/bosc

//! Provide the various constants.

/// Machine type values.
pub mod machine_type {
    /// The content is assumed to be applicable to any machine type.
    pub const IMAGE_FILE_MACHINE_UNKNOWN: u16 = 0x0;
    /// Matsushita AM33.
    pub const IMAGE_FILE_MACHINE_AM33: u16 = 0x1d3;
    /// x64.
    pub const IMAGE_FILE_MACHINE_AMD64: u16 = 0x8664;
    /// ARM little endian.
    pub const IMAGE_FILE_MACHINE_ARM: u16 = 0x1c0;
    /// ARM64 little endian.
    pub const IMAGE_FILE_MACHINE_ARM64: u16 = 0xaa64;
    /// ARM Thumb-2 little endian.
    pub const IMAGE_FILE_MACHINE_ARMNT: u16 = 0x1c4;
    /// EFI byte code.
    pub const IMAGE_FILE_MACHINE_EBC: u16 = 0xebc;
    /// Intel 386 or later processors and compatible processors.
    pub const IMAGE_FILE_MACHINE_I386: u16 = 0x14c;
    /// Intel Itanium processor family.
    pub const IMAGE_FILE_MACHINE_IA64: u16 = 0x200;
    /// LoongArch 32-bit processor family.
    pub const IMAGE_FILE_MACHINE_LOONGARCH32: u16 = 0x6232;
    /// LoongArch 64-bit processor family.
    pub const IMAGE_FILE_MACHINE_LOONGARCH64: u16 = 0x6264;
    /// Mitsubishi M32R little endian.
    pub const IMAGE_FILE_MACHINE_M32R: u16 = 0x9041;
    /// MIPS16.
    pub const IMAGE_FILE_MACHINE_MIPS16: u16 = 0x266;
    /// MIPS with FPU.
    pub const IMAGE_FILE_MACHINE_MIPSFPU: u16 = 0x366;
    /// MIPS16 with FPU.
    pub const IMAGE_FILE_MACHINE_MIPSFPU16: u16 = 0x466;
    /// Power PC little endian.
    pub const IMAGE_FILE_MACHINE_POWERPC: u16 = 0x1f0;
    /// Power PC with floating point support.
    pub const IMAGE_FILE_MACHINE_POWERPCFP: u16 = 0x1f1;
    /// MIPS little endian.
    pub const IMAGE_FILE_MACHINE_R4000: u16 = 0x166;
    /// RISC-V 32-bit address space.
    pub const IMAGE_FILE_MACHINE_RISCV32: u16 = 0x5032;
    /// RISC-V 64-bit address space.
    pub const IMAGE_FILE_MACHINE_RISCV64: u16 = 0x5064;
    /// RISC-V 128-bit address space.
    pub const IMAGE_FILE_MACHINE_RISCV128: u16 = 0x5128;
    /// Hitachi SH3.
    pub const IMAGE_FILE_MACHINE_SH3: u16 = 0x1a2;
    /// Hitachi SH3 DSP.
    pub const IMAGE_FILE_MACHINE_SH3DSP: u16 = 0x1a3;
    /// Hitachi SH4.
    pub const IMAGE_FILE_MACHINE_SH4: u16 = 0x1a6;
    /// Hitachi SH5.
    pub const IMAGE_FILE_MACHINE_SH5: u16 = 0x1a8;
    /// Thumb.
    pub const IMAGE_FILE_MACHINE_THUMB: u16 = 0x1c2;
    /// MIPS little-endian WCE v2.
    pub const IMAGE_FILE_MACHINE_WCEMIPSV2: u16 = 0x169;
}

/// Characteristics flags.
pub mod characteristics {
    /// Image only, Windows CE, and Microsoft Windows NT and later. This
    /// indicates that the file does not contain base relocations and must
    /// therefore be loaded at its preferred base address. If the base
    /// address is not available, the loader reports an error. The default
    /// behavior of the linker is to strip base relocations from executable
    /// (EXE) files.
    pub const IMAGE_FILE_RELOCS_STRIPPED: u16 = 0x0001;
    /// Image only. This indicates that the image file is valid and can be
    /// run. If this flag is not set, it indicates a linker error.
    pub const IMAGE_FILE_EXECUTABLE_IMAGE: u16 = 0x0002;
    /// COFF line numbers have been removed. This flag is deprecated and
    /// should be zero.
    pub const IMAGE_FILE_LINE_NUMS_STRIPPED: u16 = 0x0004;
    /// COFF symbol table entries for local symbols have been removed.
    /// This flag is deprecated and should be zero.
    pub const IMAGE_FILE_LOCAL_SYMS_STRIPPED: u16 = 0x0008;
    /// Obsolete. Aggressively trim working set. This flag is deprecated
    /// for Windows 2000 and later and must be zero.
    pub const IMAGE_FILE_AGGRESSIVE_WS_TRIM: u16 = 0x0010;
    /// Application can handle > 2-GB addresses.
    pub const IMAGE_FILE_LARGE_ADDRESS_AWARE: u16 = 0x0020;
    /// This flag is reserved for future use.
    pub const IMAGE_RESERVED: u16 = 0x0040;
    /// Little endian: the least significant bit (LSB) precedes the most
    /// significant bit (MSB) in memory. This flag is deprecated and
    /// should be zero.
    pub const IMAGE_FILE_BYTES_REVERSED_LO: u16 = 0x0080;
    /// Machine is based on a 32-bit-word architecture.
    pub const IMAGE_FILE_32BIT_MACHINE: u16 = 0x0100;
    /// Debugging information is removed from the image file.
    pub const IMAGE_FILE_DEBUG_STRIPPED: u16 = 0x0200;
    /// If the image is on removable media, fully load it and copy it to
    /// the swap file.
    pub const IMAGE_FILE_REMOVABLE_RUN_FROM_SWAP: u16 = 0x0400;
    /// If the image is on network media, fully load it and copy it to
    /// the swap file.
    pub const IMAGE_FILE_NET_RUN_FROM_SWAP: u16 = 0x0800;
    /// The image file is a system file, not a user program.
    pub const IMAGE_FILE_SYSTEM: u16 = 0x1000;
    /// The image file is a dynamic-link library (DLL). Such files are
    /// considered executable files for almost all purposes, although
    /// they cannot be directly run.
    pub const IMAGE_FILE_DLL: u16 = 0x2000;
    /// The file should be run only on a uniprocessor machine.
    pub const IMAGE_FILE_UP_SYSTEM_ONLY: u16 = 0x4000;
    /// Big endian: the MSB precedes the LSB in memory. This flag is
    /// deprecated and should be zero.
    pub const IMAGE_FILE_BYTES_REVERSED_HI: u16 = 0x8000;
}

/// Magic numbers that specify the type of image in the optional header.
pub mod magic_number {
    /// 32-bit address space.
    pub const PE32: u16 = 0x10b;
    /// ROM image.
    pub const ROM: u16 = 0x107;
    /// Allow for a 64-bit address space and limit the image size to 2 Gib.
    pub const PE32PLUS: u16 = 0x20b;
}

/// Specify the different Windows subsystems.
pub mod windows_subsystem {
    /// An unknown subsystem
    pub const IMAGE_SUBSYSTEM_UNKNOWN: u16 = 0;
    /// Device drivers and native Windows processes
    pub const IMAGE_SUBSYSTEM_NATIVE: u16 = 1;
    /// The Windows graphical user interface (GUI) subsystem
    pub const IMAGE_SUBSYSTEM_WINDOWS_GUI: u16 = 2;
    /// The Windows character subsystem
    pub const IMAGE_SUBSYSTEM_WINDOWS_CUI: u16 = 3;
    /// The OS/2 character subsystem
    pub const IMAGE_SUBSYSTEM_OS2_CUI: u16 = 5;
    /// The Posix character subsystem
    pub const IMAGE_SUBSYSTEM_POSIX_CUI: u16 = 7;
    /// Native Win9x driver
    pub const IMAGE_SUBSYSTEM_NATIVE_WINDOWS: u16 = 8;
    /// Windows CE
    pub const IMAGE_SUBSYSTEM_WINDOWS_CE_GUI: u16 = 9;
    /// An Extensible Firmware Interface (EFI) application
    pub const IMAGE_SUBSYSTEM_EFI_APPLICATION: u16 = 10;
    /// An EFI driver with boot services
    pub const IMAGE_SUBSYSTEM_EFI_BOOT_SERVICE_DRIVER: u16 = 11;
    /// An EFI driver with run-time services
    pub const IMAGE_SUBSYSTEM_EFI_RUNTIME_DRIVER: u16 = 12;
    /// An EFI ROM image
    pub const IMAGE_SUBSYSTEM_EFI_ROM: u16 = 13;
    /// XBOX
    pub const IMAGE_SUBSYSTEM_XBOX: u16 = 14;
    /// Windows boot application.
    pub const IMAGE_SUBSYSTEM_WINDOWS_BOOT_APPLICATION: u16 = 16;
}

/// DLL characteristics.
pub mod dll_characteristics {
    /// Reserved, must be zero.
    pub const RESERVED1: u16 = 0x0001;
    /// Reserved, must be zero.
    pub const RESERVED2: u16 = 0x0002;
    /// Reserved, must be zero.
    pub const RESERVED3: u16 = 0x0004;
    /// Reserved, must be zero.
    pub const RESERVED4: u16 = 0x0008;
    /// Image can handle a high entropy 64-bit virtual address space.
    pub const IMAGE_DLLCHARACTERISTICS_HIGH_ENTROPY_VA: u16 = 0x0020;
    /// DLL can be relocated at load time.
    pub const IMAGE_DLLCHARACTERISTICS_DYNAMIC_BASE: u16 = 0x0040;
    /// Code Integrity checks are enforced.
    pub const IMAGE_DLLCHARACTERISTICS_FORCE_INTEGRITY: u16 = 0x0080;
    /// Image is NX compatible.
    pub const IMAGE_DLLCHARACTERISTICS_NX_COMPAT: u16 = 0x0100;
    /// Isolation aware, but do not isolate the image.
    pub const IMAGE_DLLCHARACTERISTICS_NO_ISOLATION: u16 = 0x0200;
    /// Does not use structured exception (SE) handling. No SE handler may be called in this image.
    pub const IMAGE_DLLCHARACTERISTICS_NO_SEH: u16 = 0x0400;
    /// Do not bind the image.
    pub const IMAGE_DLLCHARACTERISTICS_NO_BIND: u16 = 0x0800;
    /// Image must execute in an AppContainer.
    pub const IMAGE_DLLCHARACTERISTICS_APPCONTAINER: u16 = 0x1000;
    /// A WDM driver.
    pub const IMAGE_DLLCHARACTERISTICS_WDM_DRIVER: u16 = 0x2000;
    /// Image supports Control Flow Guard.
    pub const IMAGE_DLLCHARACTERISTICS_GUARD_CF: u16 = 0x4000;
    /// Terminal Server aware.
    pub const IMAGE_DLLCHARACTERISTICS_TERMINAL_SERVER_AWARE: u16 = 0x8000;
}
