// The Bosc Library
// Copyright (c) 2022 by Stacy Prowell.  All rights reserved.
// https://gitlab.com/sprowell/bosc

//! Define the header formats for COFF and PE files.

use crate::blob::Blob;
use crate::pe::constants::machine_type;
use crate::pe::constants::magic_number;
use crate::pe::constants::windows_subsystem;
use std::time::{SystemTime, UNIX_EPOCH};

// WARNING: The code in this file was generated from tables.  See the etc folder
// in ths project for coff_headers_gen.py.  Please do not make changes to this file,
// but if necessary make changes to the generator file.  Ultimately this might
// all be replaceable with Rust procedural macros.

/// Implement the COFF header.  In an object file this is the first
/// element.  In an image file this comes immediately after the
/// signature.
#[derive(Clone, Debug)]
pub struct COFFHeader {
    blob: Blob,
}

impl COFFHeader {
    /// Make a new, empty instance.  The resulting header element
    /// has no content and has size zero.
    pub fn new() -> Self {
        Self { blob: Blob::new() }
    }

    /// Make a new instance wrapping the given blob.  Careful!  No
    /// size checking is performed on the blob.
    pub fn new_from_blob(blob: Blob) -> Self {
        Self { blob }
    }

    /// Get the machine field.
    ///
    /// This is a number identifying the type of target machine.  See the
    /// [`crate::pe::constants::machine_type`] trait's associated constants.
    pub fn get_machine(&self) -> u16 {
        self.blob.get_u16(0).unwrap_or(0)
    }

    /// Set the machine field.
    ///
    /// This is a number identifying the type of target machine.  See the
    /// [`crate::pe::constants::machine_type`] trait's associated constants.
    pub fn set_machine(&mut self, value: u16) {
        self.blob.set_u16(0, value);
    }

    /// Get the number_of_sections field.
    ///
    /// The number of sections.  This provides the size of the section
    /// table that immediately follows the headers.  The Windows loader
    /// limits this number to 96, in spite of it being a 16-bit integer.
    pub fn get_number_of_sections(&self) -> u16 {
        self.blob.get_u16(0).unwrap_or(0)
    }

    /// Set the number_of_sections field.
    ///
    /// The number of sections.  This provides the size of the section
    /// table that immediately follows the headers.  The Windows loader
    /// limits this number to 96, in spite of it being a 16-bit integer.
    pub fn set_number_of_sections(&mut self, value: u16) {
        self.blob.set_u16(0, value);
    }

    /// Get the time_date_stamp field.
    ///
    /// The low 32 bites of the UNIX epoch to indicate when the file
    /// was created.  This will overflow on January 19th, 2038, but by
    /// then (perhaps) we will be using 64 bits.
    pub fn get_time_date_stamp(&self) -> u32 {
        self.blob.get_u32(0).unwrap_or(0)
    }

    /// Set the time_date_stamp field.
    ///
    /// The low 32 bites of the UNIX epoch to indicate when the file
    /// was created.  This will overflow on January 19th, 2038, but by
    /// then (perhaps) we will be using 64 bits.
    pub fn set_time_date_stamp(&mut self, value: u32) {
        self.blob.set_u32(0, value);
    }

    /// Get the pointer_to_symbol_table field.
    ///
    /// The file offset of the COFF symbol table.  If this is zero,
    /// then no symbol table is present.  The Microsoft documentation
    /// says this should be zero for an image since COFF debugging
    /// information is deprecated.
    pub fn get_pointer_to_symbol_table(&self) -> u32 {
        self.blob.get_u32(0).unwrap_or(0)
    }

    /// Set the pointer_to_symbol_table field.
    ///
    /// The file offset of the COFF symbol table.  If this is zero,
    /// then no symbol table is present.  The Microsoft documentation
    /// says this should be zero for an image since COFF debugging
    /// information is deprecated.
    pub fn set_pointer_to_symbol_table(&mut self, value: u32) {
        self.blob.set_u32(0, value);
    }

    /// Get the number_of_symbols field.
    ///
    /// The number of symbol entries in the symbol table.  This data
    /// can be used to locate the string table, which immediately follows
    /// the symbol table.  The Microsoft documentation says this should
    /// be zero for an image since COFF debugging information is deprecated.
    pub fn get_number_of_symbols(&self) -> u32 {
        self.blob.get_u32(0).unwrap_or(0)
    }

    /// Set the number_of_symbols field.
    ///
    /// The number of symbol entries in the symbol table.  This data
    /// can be used to locate the string table, which immediately follows
    /// the symbol table.  The Microsoft documentation says this should
    /// be zero for an image since COFF debugging information is deprecated.
    pub fn set_number_of_symbols(&mut self, value: u32) {
        self.blob.set_u32(0, value);
    }

    /// Get the size_of_optional_header field.
    ///
    /// The size of the optional header, which is required for
    /// executable files but not for object files.  This value should be
    /// zero for an object file.  This optional header is defined by
    /// [`OptionalStandardHeader`], [`OptionalWindowsHeader32`],
    /// and [`OptionalWindowsHeader32Plus`].
    pub fn get_size_of_optional_header(&self) -> u16 {
        self.blob.get_u16(0).unwrap_or(0)
    }

    /// Set the size_of_optional_header field.
    ///
    /// The size of the optional header, which is required for
    /// executable files but not for object files.  This value should be
    /// zero for an object file.  This optional header is defined by
    /// [`OptionalStandardHeader`], [`OptionalWindowsHeader32`],
    /// and [`OptionalWindowsHeader32Plus`].
    pub fn set_size_of_optional_header(&mut self, value: u16) {
        self.blob.set_u16(0, value);
    }

    /// Get the characteristics field.
    ///
    /// The flags that indicate the attributes of teh file.  For specific
    /// flag values, see [`crate::pe::constants::characteristics`].
    pub fn get_characteristics(&self) -> u16 {
        self.blob.get_u16(0).unwrap_or(0)
    }

    /// Set the characteristics field.
    ///
    /// The flags that indicate the attributes of teh file.  For specific
    /// flag values, see [`crate::pe::constants::characteristics`].
    pub fn set_characteristics(&mut self, value: u16) {
        self.blob.set_u16(0, value);
    }

    /// Get information about a field by name.  If the given name is
    /// present in the header definition, then the returned value will
    /// be a pair of the zero-based offset into the header (not the file)
    /// and the width of the field in bytes.
    ///
    /// If the named field is not present, then the pair (0,0) is returned.
    pub fn get_spec(name: &str) -> (usize, usize) {
        match name {
            "machine" => (0, 2),
            "number_of_sections" => (2, 2),
            "time_date_stamp" => (4, 4),
            "pointer_to_symbol_table" => (8, 4),
            "number_of_symbols" => (12, 4),
            "size_of_optional_header" => (16, 2),
            "characteristics" => (18, 2),
            _ => (0, 0),
        }
    }

    /// Obtain a mutable reference into the underlying blob.
    pub fn raw_mut(&mut self) -> &mut Blob {
        &mut self.blob
    }

    /// Obtain a non-mutable reference into the underlying blob.
    pub fn raw(&self) -> &Blob {
        &self.blob
    }

    /// Total size of the header, in bytes.
    pub const SIZE: usize = 20;
}

impl Default for COFFHeader {
    fn default() -> Self {
        let mut header = Self::new();
        header.set_machine(machine_type::IMAGE_FILE_MACHINE_I386);
        header.set_number_of_sections(0);
        header.set_time_date_stamp({
            // Get the current UNIX time/date value.
            SystemTime::now()
                .duration_since(UNIX_EPOCH)
                .unwrap()
                .as_secs() as u32
        });
        header.set_pointer_to_symbol_table(0);
        header.set_number_of_symbols(0);
        header.set_size_of_optional_header(0);
        header.set_characteristics(0);
        header
    }
}

impl From<Blob> for COFFHeader {
    fn from(blob: Blob) -> Self {
        Self::new_from_blob(blob)
    }
}

impl From<COFFHeader> for Blob {
    fn from(header: COFFHeader) -> Blob {
        header.blob
    }
}

/// The standard part of the optional header.  These are defined for every
/// implementation of COFF (as per Microsoft).
#[derive(Clone, Debug)]
pub struct OptionalStandardHeader {
    blob: Blob,
}

impl OptionalStandardHeader {
    /// Make a new, empty instance.  The resulting header element
    /// has no content and has size zero.
    pub fn new() -> Self {
        Self { blob: Blob::new() }
    }

    /// Make a new instance wrapping the given blob.  Careful!  No
    /// size checking is performed on the blob.
    pub fn new_from_blob(blob: Blob) -> Self {
        Self { blob }
    }

    /// Get the magic field.
    ///
    /// Specify the type of image file.  The known types are found in
    /// [`crate::pe::constants::magic_number`].
    pub fn get_magic(&self) -> u16 {
        self.blob.get_u16(0).unwrap_or(0)
    }

    /// Set the magic field.
    ///
    /// Specify the type of image file.  The known types are found in
    /// [`crate::pe::constants::magic_number`].
    pub fn set_magic(&mut self, value: u16) {
        self.blob.set_u16(0, value);
    }

    /// Get the major_linker_version field.
    ///
    /// Linker major version number.
    pub fn get_major_linker_version(&self) -> u8 {
        self.blob.get_u8(0).unwrap_or(0)
    }

    /// Set the major_linker_version field.
    ///
    /// Linker major version number.
    pub fn set_major_linker_version(&mut self, value: u8) {
        self.blob.set_u8(0, value);
    }

    /// Get the minor_linker_version field.
    ///
    /// Linker minor version number.
    pub fn get_minor_linker_version(&self) -> u8 {
        self.blob.get_u8(0).unwrap_or(0)
    }

    /// Set the minor_linker_version field.
    ///
    /// Linker minor version number.
    pub fn set_minor_linker_version(&mut self, value: u8) {
        self.blob.set_u8(0, value);
    }

    /// Get the size_of_code field.
    ///
    /// The size of the code.  This is the sum of the size of all the text sections.
    pub fn get_size_of_code(&self) -> u32 {
        self.blob.get_u32(0).unwrap_or(0)
    }

    /// Set the size_of_code field.
    ///
    /// The size of the code.  This is the sum of the size of all the text sections.
    pub fn set_size_of_code(&mut self, value: u32) {
        self.blob.set_u32(0, value);
    }

    /// Get the size_of_initialized_data field.
    ///
    /// The size of the initialized data.  This is the size of all the data sections.
    pub fn get_size_of_initialized_data(&self) -> u32 {
        self.blob.get_u32(0).unwrap_or(0)
    }

    /// Set the size_of_initialized_data field.
    ///
    /// The size of the initialized data.  This is the size of all the data sections.
    pub fn set_size_of_initialized_data(&mut self, value: u32) {
        self.blob.set_u32(0, value);
    }

    /// Get the size_of_uninitialized_data field.
    ///
    /// Ths size of the uninitialized data.  This is the size of all the bss sections.
    pub fn get_size_of_uninitialized_data(&self) -> u32 {
        self.blob.get_u32(0).unwrap_or(0)
    }

    /// Set the size_of_uninitialized_data field.
    ///
    /// Ths size of the uninitialized data.  This is the size of all the bss sections.
    pub fn set_size_of_uninitialized_data(&mut self, value: u32) {
        self.blob.set_u32(0, value);
    }

    /// Get the address_of_entry_point field.
    ///
    /// The address of the entry point relative to the image base when the
    /// executable file is loaded into memory. For program images, this is
    /// the starting address. For device drivers, this is the address of the
    /// initialization function. An entry point is optional for DLLs. When
    /// no entry point is present, this field must be zero.
    pub fn get_address_of_entry_point(&self) -> u32 {
        self.blob.get_u32(0).unwrap_or(0)
    }

    /// Set the address_of_entry_point field.
    ///
    /// The address of the entry point relative to the image base when the
    /// executable file is loaded into memory. For program images, this is
    /// the starting address. For device drivers, this is the address of the
    /// initialization function. An entry point is optional for DLLs. When
    /// no entry point is present, this field must be zero.
    pub fn set_address_of_entry_point(&mut self, value: u32) {
        self.blob.set_u32(0, value);
    }

    /// Get the base_of_code field.
    ///
    /// The address that is relative to the image base of the
    /// beginning-of-code section when it is loaded into memory.
    pub fn get_base_of_code(&self) -> u32 {
        self.blob.get_u32(0).unwrap_or(0)
    }

    /// Set the base_of_code field.
    ///
    /// The address that is relative to the image base of the
    /// beginning-of-code section when it is loaded into memory.
    pub fn set_base_of_code(&mut self, value: u32) {
        self.blob.set_u32(0, value);
    }

    /// Get the base_of_data field.
    ///
    /// The address that is relative to the image base of the
    /// beginning-of-data section when it is loaded into memory.
    /// Note that this item is only included in the file for PE32+
    /// files.
    pub fn get_base_of_data(&self) -> u32 {
        self.blob.get_u32(0).unwrap_or(0)
    }

    /// Set the base_of_data field.
    ///
    /// The address that is relative to the image base of the
    /// beginning-of-data section when it is loaded into memory.
    /// Note that this item is only included in the file for PE32+
    /// files.
    pub fn set_base_of_data(&mut self, value: u32) {
        self.blob.set_u32(0, value);
    }

    /// Get information about a field by name.  If the given name is
    /// present in the header definition, then the returned value will
    /// be a pair of the zero-based offset into the header (not the file)
    /// and the width of the field in bytes.
    ///
    /// If the named field is not present, then the pair (0,0) is returned.
    pub fn get_spec(name: &str) -> (usize, usize) {
        match name {
            "magic" => (0, 2),
            "major_linker_version" => (2, 1),
            "minor_linker_version" => (3, 1),
            "size_of_code" => (4, 4),
            "size_of_initialized_data" => (8, 4),
            "size_of_uninitialized_data" => (12, 4),
            "address_of_entry_point" => (16, 4),
            "base_of_code" => (20, 4),
            "base_of_data" => (24, 4),
            _ => (0, 0),
        }
    }

    /// Obtain a mutable reference into the underlying blob.
    pub fn raw_mut(&mut self) -> &mut Blob {
        &mut self.blob
    }

    /// Obtain a non-mutable reference into the underlying blob.
    pub fn raw(&self) -> &Blob {
        &self.blob
    }

    /// Total size of the header, in bytes.
    pub const SIZE: usize = 28;
}

impl Default for OptionalStandardHeader {
    fn default() -> Self {
        let mut header = Self::new();
        header.set_magic(magic_number::PE32);
        header.set_major_linker_version(0);
        header.set_minor_linker_version(0);
        header.set_size_of_code(0);
        header.set_size_of_initialized_data(0);
        header.set_size_of_uninitialized_data(0);
        header.set_address_of_entry_point(0);
        header.set_base_of_code(0);
        header.set_base_of_data(0);
        header
    }
}

impl From<Blob> for OptionalStandardHeader {
    fn from(blob: Blob) -> Self {
        Self::new_from_blob(blob)
    }
}

impl From<OptionalStandardHeader> for Blob {
    fn from(header: OptionalStandardHeader) -> Blob {
        header.blob
    }
}

/// Specify additional information required by the linker and loader in Windows.
#[derive(Clone, Debug)]
pub struct OptionalWindowsHeader32 {
    blob: Blob,
}

impl OptionalWindowsHeader32 {
    /// Make a new, empty instance.  The resulting header element
    /// has no content and has size zero.
    pub fn new() -> Self {
        Self { blob: Blob::new() }
    }

    /// Make a new instance wrapping the given blob.  Careful!  No
    /// size checking is performed on the blob.
    pub fn new_from_blob(blob: Blob) -> Self {
        Self { blob }
    }

    /// Get the image_base field.
    ///
    /// The preferred address of the first byte of image when loaded
    /// into memory; must be a multiple of 64 K. The default for DLLs
    /// is `0x10000000`. The default for Windows CE EXEs is `0x00010000`.
    /// The default for Windows NT, Windows 2000, Windows XP, Windows
    /// 95, Windows 98, and Windows Me is `0x00400000`.
    pub fn get_image_base(&self) -> u32 {
        self.blob.get_u32(0).unwrap_or(0)
    }

    /// Set the image_base field.
    ///
    /// The preferred address of the first byte of image when loaded
    /// into memory; must be a multiple of 64 K. The default for DLLs
    /// is `0x10000000`. The default for Windows CE EXEs is `0x00010000`.
    /// The default for Windows NT, Windows 2000, Windows XP, Windows
    /// 95, Windows 98, and Windows Me is `0x00400000`.
    pub fn set_image_base(&mut self, value: u32) {
        self.blob.set_u32(0, value);
    }

    /// Get the section_alignment field.
    ///
    /// The alignment (in bytes) of sections when they are loaded into
    /// memory. It must be greater than or equal to FileAlignment. The
    /// default is the page size for the architecture.
    pub fn get_section_alignment(&self) -> u32 {
        self.blob.get_u32(0).unwrap_or(0)
    }

    /// Set the section_alignment field.
    ///
    /// The alignment (in bytes) of sections when they are loaded into
    /// memory. It must be greater than or equal to FileAlignment. The
    /// default is the page size for the architecture.
    pub fn set_section_alignment(&mut self, value: u32) {
        self.blob.set_u32(0, value);
    }

    /// Get the file_alignment field.
    ///
    /// The alignment factor (in bytes) that is used to align the raw
    /// data of sections in the image file. The value should be a power
    /// of 2 between 512 and 64 K, inclusive. The default is 512. If
    /// the section alignment is less than the architecture's page size,
    /// then file alignment must match section alignment.
    pub fn get_file_alignment(&self) -> u32 {
        self.blob.get_u32(0).unwrap_or(0)
    }

    /// Set the file_alignment field.
    ///
    /// The alignment factor (in bytes) that is used to align the raw
    /// data of sections in the image file. The value should be a power
    /// of 2 between 512 and 64 K, inclusive. The default is 512. If
    /// the section alignment is less than the architecture's page size,
    /// then file alignment must match section alignment.
    pub fn set_file_alignment(&mut self, value: u32) {
        self.blob.set_u32(0, value);
    }

    /// Get the major_operating_system_version field.
    ///
    /// The major version number of the required operating system.
    pub fn get_major_operating_system_version(&self) -> u16 {
        self.blob.get_u16(0).unwrap_or(0)
    }

    /// Set the major_operating_system_version field.
    ///
    /// The major version number of the required operating system.
    pub fn set_major_operating_system_version(&mut self, value: u16) {
        self.blob.set_u16(0, value);
    }

    /// Get the minor_operating_system_version field.
    ///
    /// The minor version number of the required operating system.
    pub fn get_minor_operating_system_version(&self) -> u16 {
        self.blob.get_u16(0).unwrap_or(0)
    }

    /// Set the minor_operating_system_version field.
    ///
    /// The minor version number of the required operating system.
    pub fn set_minor_operating_system_version(&mut self, value: u16) {
        self.blob.set_u16(0, value);
    }

    /// Get the major_image_version field.
    ///
    /// The major version number of this image.
    pub fn get_major_image_version(&self) -> u16 {
        self.blob.get_u16(0).unwrap_or(0)
    }

    /// Set the major_image_version field.
    ///
    /// The major version number of this image.
    pub fn set_major_image_version(&mut self, value: u16) {
        self.blob.set_u16(0, value);
    }

    /// Get the minor_image_version field.
    ///
    /// The minor version number of this image.
    pub fn get_minor_image_version(&self) -> u16 {
        self.blob.get_u16(0).unwrap_or(0)
    }

    /// Set the minor_image_version field.
    ///
    /// The minor version number of this image.
    pub fn set_minor_image_version(&mut self, value: u16) {
        self.blob.set_u16(0, value);
    }

    /// Get the major_subsystem_version field.
    ///
    /// The major version number of the subsystem.
    pub fn get_major_subsystem_version(&self) -> u16 {
        self.blob.get_u16(0).unwrap_or(0)
    }

    /// Set the major_subsystem_version field.
    ///
    /// The major version number of the subsystem.
    pub fn set_major_subsystem_version(&mut self, value: u16) {
        self.blob.set_u16(0, value);
    }

    /// Get the minor_subsystem_version field.
    ///
    /// The minor version number of the subsystem.
    pub fn get_minor_subsystem_version(&self) -> u16 {
        self.blob.get_u16(0).unwrap_or(0)
    }

    /// Set the minor_subsystem_version field.
    ///
    /// The minor version number of the subsystem.
    pub fn set_minor_subsystem_version(&mut self, value: u16) {
        self.blob.set_u16(0, value);
    }

    /// Get the win32_version_value field.
    ///
    /// Reserved; must be zero.
    pub fn get_win32_version_value(&self) -> u32 {
        self.blob.get_u32(0).unwrap_or(0)
    }

    /// Set the win32_version_value field.
    ///
    /// Reserved; must be zero.
    pub fn set_win32_version_value(&mut self, value: u32) {
        self.blob.set_u32(0, value);
    }

    /// Get the size_of_image field.
    ///
    /// The size (in bytes) of the image, including all headers, as the
    /// image is loaded in memory. It must be a multiple of the section
    /// alignment.
    pub fn get_size_of_image(&self) -> u32 {
        self.blob.get_u32(0).unwrap_or(0)
    }

    /// Set the size_of_image field.
    ///
    /// The size (in bytes) of the image, including all headers, as the
    /// image is loaded in memory. It must be a multiple of the section
    /// alignment.
    pub fn set_size_of_image(&mut self, value: u32) {
        self.blob.set_u32(0, value);
    }

    /// Get the size_of_headers field.
    ///
    /// The combined size of an MS-DOS stub, PE header, and section headers
    /// rounded up to a multiple of the file alignment.
    pub fn get_size_of_headers(&self) -> u32 {
        self.blob.get_u32(0).unwrap_or(0)
    }

    /// Set the size_of_headers field.
    ///
    /// The combined size of an MS-DOS stub, PE header, and section headers
    /// rounded up to a multiple of the file alignment.
    pub fn set_size_of_headers(&mut self, value: u32) {
        self.blob.set_u32(0, value);
    }

    /// Get the check_sum field.
    ///
    /// The image file checksum. The algorithm for computing the checksum
    /// is incorporated into `IMAGHELP.DLL`. The following are checked for
    /// validation at load time: all drivers, any DLL loaded at boot time,
    /// and any DLL that is loaded into a critical Windows process.
    pub fn get_check_sum(&self) -> u32 {
        self.blob.get_u32(0).unwrap_or(0)
    }

    /// Set the check_sum field.
    ///
    /// The image file checksum. The algorithm for computing the checksum
    /// is incorporated into `IMAGHELP.DLL`. The following are checked for
    /// validation at load time: all drivers, any DLL loaded at boot time,
    /// and any DLL that is loaded into a critical Windows process.
    pub fn set_check_sum(&mut self, value: u32) {
        self.blob.set_u32(0, value);
    }

    /// Get the subsystem field.
    ///
    /// The subsystem that is required to run this image. For more information,
    /// see crate::pe::constants::windows_subsystem`].
    pub fn get_subsystem(&self) -> u16 {
        self.blob.get_u16(0).unwrap_or(0)
    }

    /// Set the subsystem field.
    ///
    /// The subsystem that is required to run this image. For more information,
    /// see crate::pe::constants::windows_subsystem`].
    pub fn set_subsystem(&mut self, value: u16) {
        self.blob.set_u16(0, value);
    }

    /// Get the dll_characteristics field.
    ///
    /// For more information see [`crate::pe::constants::dll_characteristics`].
    pub fn get_dll_characteristics(&self) -> u16 {
        self.blob.get_u16(0).unwrap_or(0)
    }

    /// Set the dll_characteristics field.
    ///
    /// For more information see [`crate::pe::constants::dll_characteristics`].
    pub fn set_dll_characteristics(&mut self, value: u16) {
        self.blob.set_u16(0, value);
    }

    /// Get the size_of_stack_reserve field.
    ///
    /// The size of the stack to reserve. Only the stack commit size
    /// is committed; the rest is made available one page at a time until
    /// the reserve size is reached.
    pub fn get_size_of_stack_reserve(&self) -> u32 {
        self.blob.get_u32(0).unwrap_or(0)
    }

    /// Set the size_of_stack_reserve field.
    ///
    /// The size of the stack to reserve. Only the stack commit size
    /// is committed; the rest is made available one page at a time until
    /// the reserve size is reached.
    pub fn set_size_of_stack_reserve(&mut self, value: u32) {
        self.blob.set_u32(0, value);
    }

    /// Get the size_of_stack_commit field.
    ///
    /// The size of the stack to commit.
    pub fn get_size_of_stack_commit(&self) -> u32 {
        self.blob.get_u32(0).unwrap_or(0)
    }

    /// Set the size_of_stack_commit field.
    ///
    /// The size of the stack to commit.
    pub fn set_size_of_stack_commit(&mut self, value: u32) {
        self.blob.set_u32(0, value);
    }

    /// Get the size_of_heap_reserve field.
    ///
    /// The size of the local heap space to reserve. Only the heap commit size
    /// is committed; the rest is made available one page at a time until
    /// the reserve size is reached.
    pub fn get_size_of_heap_reserve(&self) -> u32 {
        self.blob.get_u32(0).unwrap_or(0)
    }

    /// Set the size_of_heap_reserve field.
    ///
    /// The size of the local heap space to reserve. Only the heap commit size
    /// is committed; the rest is made available one page at a time until
    /// the reserve size is reached.
    pub fn set_size_of_heap_reserve(&mut self, value: u32) {
        self.blob.set_u32(0, value);
    }

    /// Get the size_of_heap_commit field.
    ///
    /// The size of the local heap space to commit.
    pub fn get_size_of_heap_commit(&self) -> u32 {
        self.blob.get_u32(0).unwrap_or(0)
    }

    /// Set the size_of_heap_commit field.
    ///
    /// The size of the local heap space to commit.
    pub fn set_size_of_heap_commit(&mut self, value: u32) {
        self.blob.set_u32(0, value);
    }

    /// Get the loader_flags field.
    ///
    /// Reserved; must be zero.
    pub fn get_loader_flags(&self) -> u32 {
        self.blob.get_u32(0).unwrap_or(0)
    }

    /// Set the loader_flags field.
    ///
    /// Reserved; must be zero.
    pub fn set_loader_flags(&mut self, value: u32) {
        self.blob.set_u32(0, value);
    }

    /// Get the number_of_rva_and_sizes field.
    ///
    /// The number of data-directory entries in the remainder of the
    /// optional header. Each describes a location and size.
    pub fn get_number_of_rva_and_sizes(&self) -> u32 {
        self.blob.get_u32(0).unwrap_or(0)
    }

    /// Set the number_of_rva_and_sizes field.
    ///
    /// The number of data-directory entries in the remainder of the
    /// optional header. Each describes a location and size.
    pub fn set_number_of_rva_and_sizes(&mut self, value: u32) {
        self.blob.set_u32(0, value);
    }

    /// Get information about a field by name.  If the given name is
    /// present in the header definition, then the returned value will
    /// be a pair of the zero-based offset into the header (not the file)
    /// and the width of the field in bytes.
    ///
    /// If the named field is not present, then the pair (0,0) is returned.
    pub fn get_spec(name: &str) -> (usize, usize) {
        match name {
            "image_base" => (0, 4),
            "section_alignment" => (4, 4),
            "file_alignment" => (8, 4),
            "major_operating_system_version" => (12, 2),
            "minor_operating_system_version" => (14, 2),
            "major_image_version" => (16, 2),
            "minor_image_version" => (18, 2),
            "major_subsystem_version" => (20, 2),
            "minor_subsystem_version" => (22, 2),
            "win32_version_value" => (24, 4),
            "size_of_image" => (28, 4),
            "size_of_headers" => (32, 4),
            "check_sum" => (36, 4),
            "subsystem" => (40, 2),
            "dll_characteristics" => (42, 2),
            "size_of_stack_reserve" => (44, 4),
            "size_of_stack_commit" => (48, 4),
            "size_of_heap_reserve" => (52, 4),
            "size_of_heap_commit" => (56, 4),
            "loader_flags" => (60, 4),
            "number_of_rva_and_sizes" => (64, 4),
            _ => (0, 0),
        }
    }

    /// Obtain a mutable reference into the underlying blob.
    pub fn raw_mut(&mut self) -> &mut Blob {
        &mut self.blob
    }

    /// Obtain a non-mutable reference into the underlying blob.
    pub fn raw(&self) -> &Blob {
        &self.blob
    }

    /// Total size of the header, in bytes.
    pub const SIZE: usize = 68;
}

impl Default for OptionalWindowsHeader32 {
    fn default() -> Self {
        let mut header = Self::new();
        header.set_image_base(0x00400000);
        header.set_section_alignment(4096);
        header.set_file_alignment(512);
        header.set_major_operating_system_version(0);
        header.set_minor_operating_system_version(0);
        header.set_major_image_version(0);
        header.set_minor_image_version(0);
        header.set_major_subsystem_version(0);
        header.set_minor_subsystem_version(0);
        header.set_win32_version_value(0);
        header.set_size_of_image(0);
        header.set_size_of_headers(0);
        header.set_check_sum(0);
        header.set_subsystem(windows_subsystem::IMAGE_SUBSYSTEM_NATIVE_WINDOWS);
        header.set_dll_characteristics(0);
        header.set_size_of_stack_reserve(0);
        header.set_size_of_stack_commit(0);
        header.set_size_of_heap_reserve(0);
        header.set_size_of_heap_commit(0);
        header.set_loader_flags(0);
        header.set_number_of_rva_and_sizes(0);
        header
    }
}

impl From<Blob> for OptionalWindowsHeader32 {
    fn from(blob: Blob) -> Self {
        Self::new_from_blob(blob)
    }
}

impl From<OptionalWindowsHeader32> for Blob {
    fn from(header: OptionalWindowsHeader32) -> Blob {
        header.blob
    }
}

/// Specify additional information required by the linker and loader in
/// Windows.
#[derive(Clone, Debug)]
pub struct OptionalWindowsHeader32Plus {
    blob: Blob,
}

impl OptionalWindowsHeader32Plus {
    /// Make a new, empty instance.  The resulting header element
    /// has no content and has size zero.
    pub fn new() -> Self {
        Self { blob: Blob::new() }
    }

    /// Make a new instance wrapping the given blob.  Careful!  No
    /// size checking is performed on the blob.
    pub fn new_from_blob(blob: Blob) -> Self {
        Self { blob }
    }

    /// Get the image_base field.
    ///
    /// The preferred address of the first byte of image when loaded
    /// into memory; must be a multiple of 64 K. The default for DLLs
    /// is `0x10000000`. The default for Windows CE EXEs is `0x00010000`.
    /// The default for Windows NT, Windows 2000, Windows XP, Windows
    /// 95, Windows 98, and Windows Me is `0x00400000`.
    pub fn get_image_base(&self) -> u64 {
        self.blob.get_u64(0).unwrap_or(0)
    }

    /// Set the image_base field.
    ///
    /// The preferred address of the first byte of image when loaded
    /// into memory; must be a multiple of 64 K. The default for DLLs
    /// is `0x10000000`. The default for Windows CE EXEs is `0x00010000`.
    /// The default for Windows NT, Windows 2000, Windows XP, Windows
    /// 95, Windows 98, and Windows Me is `0x00400000`.
    pub fn set_image_base(&mut self, value: u64) {
        self.blob.set_u64(0, value);
    }

    /// Get the section_alignment field.
    ///
    /// The alignment (in bytes) of sections when they are loaded into
    /// memory. It must be greater than or equal to FileAlignment. The
    /// default is the page size for the architecture.
    pub fn get_section_alignment(&self) -> u32 {
        self.blob.get_u32(0).unwrap_or(0)
    }

    /// Set the section_alignment field.
    ///
    /// The alignment (in bytes) of sections when they are loaded into
    /// memory. It must be greater than or equal to FileAlignment. The
    /// default is the page size for the architecture.
    pub fn set_section_alignment(&mut self, value: u32) {
        self.blob.set_u32(0, value);
    }

    /// Get the file_alignment field.
    ///
    /// The alignment factor (in bytes) that is used to align the raw
    /// data of sections in the image file. The value should be a power
    /// of 2 between 512 and 64 K, inclusive. The default is 512. If
    /// the section alignment is less than the architecture's page size,
    /// then file alignment must match section alignment.
    pub fn get_file_alignment(&self) -> u32 {
        self.blob.get_u32(0).unwrap_or(0)
    }

    /// Set the file_alignment field.
    ///
    /// The alignment factor (in bytes) that is used to align the raw
    /// data of sections in the image file. The value should be a power
    /// of 2 between 512 and 64 K, inclusive. The default is 512. If
    /// the section alignment is less than the architecture's page size,
    /// then file alignment must match section alignment.
    pub fn set_file_alignment(&mut self, value: u32) {
        self.blob.set_u32(0, value);
    }

    /// Get the major_operating_system_version field.
    ///
    /// The major version number of the required operating system.
    pub fn get_major_operating_system_version(&self) -> u16 {
        self.blob.get_u16(0).unwrap_or(0)
    }

    /// Set the major_operating_system_version field.
    ///
    /// The major version number of the required operating system.
    pub fn set_major_operating_system_version(&mut self, value: u16) {
        self.blob.set_u16(0, value);
    }

    /// Get the minor_operating_system_version field.
    ///
    /// The minor version number of the required operating system.
    pub fn get_minor_operating_system_version(&self) -> u16 {
        self.blob.get_u16(0).unwrap_or(0)
    }

    /// Set the minor_operating_system_version field.
    ///
    /// The minor version number of the required operating system.
    pub fn set_minor_operating_system_version(&mut self, value: u16) {
        self.blob.set_u16(0, value);
    }

    /// Get the major_image_version field.
    ///
    /// The major version number of this image.
    pub fn get_major_image_version(&self) -> u16 {
        self.blob.get_u16(0).unwrap_or(0)
    }

    /// Set the major_image_version field.
    ///
    /// The major version number of this image.
    pub fn set_major_image_version(&mut self, value: u16) {
        self.blob.set_u16(0, value);
    }

    /// Get the minor_image_version field.
    ///
    /// The minor version number of this image.
    pub fn get_minor_image_version(&self) -> u16 {
        self.blob.get_u16(0).unwrap_or(0)
    }

    /// Set the minor_image_version field.
    ///
    /// The minor version number of this image.
    pub fn set_minor_image_version(&mut self, value: u16) {
        self.blob.set_u16(0, value);
    }

    /// Get the major_subsystem_version field.
    ///
    /// The major version number of the subsystem.
    pub fn get_major_subsystem_version(&self) -> u16 {
        self.blob.get_u16(0).unwrap_or(0)
    }

    /// Set the major_subsystem_version field.
    ///
    /// The major version number of the subsystem.
    pub fn set_major_subsystem_version(&mut self, value: u16) {
        self.blob.set_u16(0, value);
    }

    /// Get the minor_subsystem_version field.
    ///
    /// The minor version number of the subsystem.
    pub fn get_minor_subsystem_version(&self) -> u16 {
        self.blob.get_u16(0).unwrap_or(0)
    }

    /// Set the minor_subsystem_version field.
    ///
    /// The minor version number of the subsystem.
    pub fn set_minor_subsystem_version(&mut self, value: u16) {
        self.blob.set_u16(0, value);
    }

    /// Get the win32_version_value field.
    ///
    /// Reserved; must be zero.
    pub fn get_win32_version_value(&self) -> u32 {
        self.blob.get_u32(0).unwrap_or(0)
    }

    /// Set the win32_version_value field.
    ///
    /// Reserved; must be zero.
    pub fn set_win32_version_value(&mut self, value: u32) {
        self.blob.set_u32(0, value);
    }

    /// Get the size_of_image field.
    ///
    /// The size (in bytes) of the image, including all headers, as the
    /// image is loaded in memory. It must be a multiple of the section alignment.
    pub fn get_size_of_image(&self) -> u32 {
        self.blob.get_u32(0).unwrap_or(0)
    }

    /// Set the size_of_image field.
    ///
    /// The size (in bytes) of the image, including all headers, as the
    /// image is loaded in memory. It must be a multiple of the section alignment.
    pub fn set_size_of_image(&mut self, value: u32) {
        self.blob.set_u32(0, value);
    }

    /// Get the size_of_headers field.
    ///
    /// The combined size of an MS-DOS stub, PE header, and section headers
    /// rounded up to a multiple of the file alignment.
    pub fn get_size_of_headers(&self) -> u32 {
        self.blob.get_u32(0).unwrap_or(0)
    }

    /// Set the size_of_headers field.
    ///
    /// The combined size of an MS-DOS stub, PE header, and section headers
    /// rounded up to a multiple of the file alignment.
    pub fn set_size_of_headers(&mut self, value: u32) {
        self.blob.set_u32(0, value);
    }

    /// Get the check_sum field.
    ///
    /// The image file checksum. The algorithm for computing the checksum
    /// is incorporated into `IMAGHELP.DLL`. The following are checked for
    /// validation at load time: all drivers, any DLL loaded at boot time,
    /// and any DLL that is loaded into a critical Windows process.
    pub fn get_check_sum(&self) -> u32 {
        self.blob.get_u32(0).unwrap_or(0)
    }

    /// Set the check_sum field.
    ///
    /// The image file checksum. The algorithm for computing the checksum
    /// is incorporated into `IMAGHELP.DLL`. The following are checked for
    /// validation at load time: all drivers, any DLL loaded at boot time,
    /// and any DLL that is loaded into a critical Windows process.
    pub fn set_check_sum(&mut self, value: u32) {
        self.blob.set_u32(0, value);
    }

    /// Get the subsystem field.
    ///
    /// The subsystem that is required to run this image. For more information,
    /// see crate::pe::constants::windows_subsystem`].
    pub fn get_subsystem(&self) -> u16 {
        self.blob.get_u16(0).unwrap_or(0)
    }

    /// Set the subsystem field.
    ///
    /// The subsystem that is required to run this image. For more information,
    /// see crate::pe::constants::windows_subsystem`].
    pub fn set_subsystem(&mut self, value: u16) {
        self.blob.set_u16(0, value);
    }

    /// Get the dll_characteristics field.
    ///
    /// For more information see [`crate::pe::constants::dll_characteristics`].
    pub fn get_dll_characteristics(&self) -> u16 {
        self.blob.get_u16(0).unwrap_or(0)
    }

    /// Set the dll_characteristics field.
    ///
    /// For more information see [`crate::pe::constants::dll_characteristics`].
    pub fn set_dll_characteristics(&mut self, value: u16) {
        self.blob.set_u16(0, value);
    }

    /// Get the size_of_stack_reserve field.
    ///
    /// The size of the stack to reserve. Only the stack commit size
    /// is committed; the rest is made available one page at a time until
    /// the reserve size is reached.
    pub fn get_size_of_stack_reserve(&self) -> u64 {
        self.blob.get_u64(0).unwrap_or(0)
    }

    /// Set the size_of_stack_reserve field.
    ///
    /// The size of the stack to reserve. Only the stack commit size
    /// is committed; the rest is made available one page at a time until
    /// the reserve size is reached.
    pub fn set_size_of_stack_reserve(&mut self, value: u64) {
        self.blob.set_u64(0, value);
    }

    /// Get the size_of_stack_commit field.
    ///
    /// The size of the stack to commit.
    pub fn get_size_of_stack_commit(&self) -> u64 {
        self.blob.get_u64(0).unwrap_or(0)
    }

    /// Set the size_of_stack_commit field.
    ///
    /// The size of the stack to commit.
    pub fn set_size_of_stack_commit(&mut self, value: u64) {
        self.blob.set_u64(0, value);
    }

    /// Get the size_of_heap_reserve field.
    ///
    /// The size of the local heap space to reserve. Only the heap commit size
    /// is committed; the rest is made available one page at a time until
    /// the reserve size is reached.
    pub fn get_size_of_heap_reserve(&self) -> u64 {
        self.blob.get_u64(0).unwrap_or(0)
    }

    /// Set the size_of_heap_reserve field.
    ///
    /// The size of the local heap space to reserve. Only the heap commit size
    /// is committed; the rest is made available one page at a time until
    /// the reserve size is reached.
    pub fn set_size_of_heap_reserve(&mut self, value: u64) {
        self.blob.set_u64(0, value);
    }

    /// Get the size_of_heap_commit field.
    ///
    /// The size of the local heap space to commit.
    pub fn get_size_of_heap_commit(&self) -> u64 {
        self.blob.get_u64(0).unwrap_or(0)
    }

    /// Set the size_of_heap_commit field.
    ///
    /// The size of the local heap space to commit.
    pub fn set_size_of_heap_commit(&mut self, value: u64) {
        self.blob.set_u64(0, value);
    }

    /// Get the loader_flags field.
    ///
    /// Reserved; must be zero.
    pub fn get_loader_flags(&self) -> u32 {
        self.blob.get_u32(0).unwrap_or(0)
    }

    /// Set the loader_flags field.
    ///
    /// Reserved; must be zero.
    pub fn set_loader_flags(&mut self, value: u32) {
        self.blob.set_u32(0, value);
    }

    /// Get the number_of_rva_and_sizes field.
    ///
    /// The number of data-directory entries in the remainder of the
    /// optional header. Each describes a location and size.
    pub fn get_number_of_rva_and_sizes(&self) -> u32 {
        self.blob.get_u32(0).unwrap_or(0)
    }

    /// Set the number_of_rva_and_sizes field.
    ///
    /// The number of data-directory entries in the remainder of the
    /// optional header. Each describes a location and size.
    pub fn set_number_of_rva_and_sizes(&mut self, value: u32) {
        self.blob.set_u32(0, value);
    }

    /// Get information about a field by name.  If the given name is
    /// present in the header definition, then the returned value will
    /// be a pair of the zero-based offset into the header (not the file)
    /// and the width of the field in bytes.
    ///
    /// If the named field is not present, then the pair (0,0) is returned.
    pub fn get_spec(name: &str) -> (usize, usize) {
        match name {
            "image_base" => (0, 8),
            "section_alignment" => (8, 4),
            "file_alignment" => (12, 4),
            "major_operating_system_version" => (16, 2),
            "minor_operating_system_version" => (18, 2),
            "major_image_version" => (20, 2),
            "minor_image_version" => (22, 2),
            "major_subsystem_version" => (24, 2),
            "minor_subsystem_version" => (26, 2),
            "win32_version_value" => (28, 4),
            "size_of_image" => (32, 4),
            "size_of_headers" => (36, 4),
            "check_sum" => (40, 4),
            "subsystem" => (44, 2),
            "dll_characteristics" => (46, 2),
            "size_of_stack_reserve" => (48, 8),
            "size_of_stack_commit" => (56, 8),
            "size_of_heap_reserve" => (64, 8),
            "size_of_heap_commit" => (72, 8),
            "loader_flags" => (80, 4),
            "number_of_rva_and_sizes" => (84, 4),
            _ => (0, 0),
        }
    }

    /// Obtain a mutable reference into the underlying blob.
    pub fn raw_mut(&mut self) -> &mut Blob {
        &mut self.blob
    }

    /// Obtain a non-mutable reference into the underlying blob.
    pub fn raw(&self) -> &Blob {
        &self.blob
    }

    /// Total size of the header, in bytes.
    pub const SIZE: usize = 88;
}

impl Default for OptionalWindowsHeader32Plus {
    fn default() -> Self {
        let mut header = Self::new();
        header.set_image_base(0x00400000);
        header.set_section_alignment(4096);
        header.set_file_alignment(512);
        header.set_major_operating_system_version(0);
        header.set_minor_operating_system_version(0);
        header.set_major_image_version(0);
        header.set_minor_image_version(0);
        header.set_major_subsystem_version(0);
        header.set_minor_subsystem_version(0);
        header.set_win32_version_value(0);
        header.set_size_of_image(0);
        header.set_size_of_headers(0);
        header.set_check_sum(0);
        header.set_subsystem(windows_subsystem::IMAGE_SUBSYSTEM_NATIVE_WINDOWS);
        header.set_dll_characteristics(0);
        header.set_size_of_stack_reserve(0);
        header.set_size_of_stack_commit(0);
        header.set_size_of_heap_reserve(0);
        header.set_size_of_heap_commit(0);
        header.set_loader_flags(0);
        header.set_number_of_rva_and_sizes(0);
        header
    }
}

impl From<Blob> for OptionalWindowsHeader32Plus {
    fn from(blob: Blob) -> Self {
        Self::new_from_blob(blob)
    }
}

impl From<OptionalWindowsHeader32Plus> for Blob {
    fn from(header: OptionalWindowsHeader32Plus) -> Blob {
        header.blob
    }
}
