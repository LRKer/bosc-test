// The Bosc Library
// Copyright (c) 2022 by Stacy Prowell.  All rights reserved.
// https://gitlab.com/sprowell/bosc

//! This module implements the Microsoft Portable Executable file
//! format and all associated pieces.

pub mod coff_headers;
pub mod constants;
