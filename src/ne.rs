// The Bosc Library
// Copyright (c) 2022 by Stacy Prowell.  All rights reserved.
// https://gitlab.com/sprowell/bosc

//! Implement the DOS NE file structure, as best we can.  The NE file structure
//! was used by Windows 1.0 through Windows 3 up to Windows 3.1, when the PE
//! format was introduced.  The file format is only partially documented.

// TODO This may be implemented or might not be.  For now this is a placeholder.
