// The Bosc Library
// Copyright (c) 2022 by Stacy Prowell.  All rights reserved.
// https://gitlab.com/sprowell/bosc

//! Implement the DOS MZ file structure.
//!
//! Important elements here are the [`MZFile`] and [`MZHeader`].  To get
//! started, create a new [`MZFile`] with [`MZFile::new()`], and then use
//! method to add elements to it.

use crate::blob::{Blob, BlobBuilder, BlobIterator};
use std::mem;

/// The MZ header structure.  This is used for DOS files only, and is
/// ignored in more modern file formats, but is still present.
///
/// The first 28 bytes are the fixed header, and they have fixed
/// positions.  All entries in this are little endian.
///
/// Sources used:
///
///   * <http://www.tavi.co.uk/phobos/exeformat.html>
///   * <https://wiki.osdev.org/MZ>
///   * <http://www.delorie.com/djgpp/doc/exe/>
///
#[derive(Debug, Clone, Copy, Default)]
pub struct MZHeader {
    /// Magic number, offset 0x0.
    ///
    /// This is also referred to as the signature word.  In fact, DOS checks
    /// this instead of relying on the `.exe` file extension.  It is two bytes,
    /// and they are usually "MZ" for
    /// [Mark Zibowski](https://en.wikipedia.org/wiki/Mark_Zbikowski)
    /// who designed the file format, but sometimes they are "ZM" (probably
    /// from poorly-written code).
    pub e_magic: [u8; 2],

    /// Size of the last page of the file, offset 0x2.
    ///
    /// Pages are 512 bytes, so this will be a number from 0 to 511.  Note
    /// that zero indicates that the last page is actually a full page (512
    /// bytes).  A way to think of this is that if this value is not zero,
    /// then the last page is only partially used, and this is the number of
    /// bytes.
    pub e_cblp: u16,

    /// Number of pages in the file, offset 0x4.
    ///
    /// Pages are 512 bytes.  The last page might not be full, so you need
    /// to check `e_cblp` to determine the size of the last page.  For a
    /// 1024 byte file, you would have `e_cp = 2` and `e_cblp = 0`.  For a
    /// 1023 byte file, you would have `e_cp = 2` and `e_cblp = 511`.
    pub e_cp: u16,

    /// Relocation items, offset 0x6.
    ///
    /// This is the number of entries in the relocation pointer table.  This
    /// value can be, and often is, zero when there are no relocations.
    pub e_crlc: u16,

    /// Size of header in paragraphs, offset 0x8.
    ///
    /// A paragraph is 16 bytes because of memory segmentation in the x86
    /// processor.
    ///
    /// This is the size of the header in paragraphs.  This value times 16
    /// (or shifted left by 4) is the size of the header.
    ///
    /// For reasons that I don't know, the header always spans an even
    /// number of paragraphs.  This implementation enforces that, but
    /// you can, if course, override it.
    ///
    /// Another way to think of this is that it gives the offset (when
    /// multiplied by 16, of course) of the program's compiled, assembled,
    /// and linked image, called the "load module."  Note that this means
    /// the image always appears on a paragraph boundary.
    pub e_cparhdr: u16,

    /// Minimum extra paragraphs needed, offset 0xa.
    ///
    /// A paragraph is 16 bytes because of memory segmentation in the x86
    /// processor.
    ///
    /// Once the load image is in memory, this is the additional memory to
    /// allocate.  This is used for uninitiazlied data and stack space.  If
    /// this space it not available, then the program cannot be loaded.
    pub e_minalloc: u16,

    /// Maximum extra paragraphs needed, offset 0xc.
    ///
    /// A paragraph is 16 bytes because of memory segmentation in the x86
    /// processor.
    ///
    /// This is the "it would be nice if..." setting.  The program would
    /// like this number of additional paragraphs to be allocated before it
    /// begins execution.  If the request cannot be satisfied, then the
    /// program is allocated whatever memory is available.
    pub e_maxalloc: u16,

    /// Initial (relative) stack segment (SS) value, offset 0xe.
    ///
    /// A paragraph is 16 bytes because of memory segmentation in the x86
    /// processor.
    ///
    /// This is the paragraph address of the stack segment relative to the
    /// start of the load module.  The segment address of the load module
    /// is added to this to obtain the segment address of the stack, which
    /// is then loaded into the SS register before program start.
    pub e_ss: u16,

    /// Initial SP value, offset 0x10.
    ///
    /// This is the value to load into the SP register before program start.
    /// It determines the stack pointer value within the stack segment and,
    /// in that sense, is an absolute value.
    pub e_sp: u16,

    /// Checksum, offset 0x12.
    ///
    /// This is the checksum of the entire file.  It is not normally
    /// checked, and can be anything.  This library attempts to compute
    /// the checksum correctly, but you can of course override it.
    ///
    /// The checksum is the ones' complement of the summation of all words
    /// in the file, excluding the checksum itself.  Just sum up all the
    /// words of the file and then complement it.
    ///
    /// ```c
    /// // Compute checksum from file data, given file length.
    /// uint16_t get_checksum( usize length, uint8_t *data )
    /// {
    ///   uint16_t cksum = 0;
    ///   for (usize offset = 0; offset < length; offset += 2) {
    ///     // Skip the checksum.  If this is initialized to zero then you
    ///     // don't need to do this.
    ///     if (offset == 0x12) continue;
    ///     // The file might have an uneven length, so handle that by
    ///     // treating the file as even with a last byte of zero.
    ///     uint16_t word = (uint16_t) data[offset] << 8;
    ///     if (offset + 1 < length) {
    ///       word |= data[offset+1];
    ///     }
    ///     cksum += word;
    ///   } // Loop over bytes of file.
    ///   cksum = 0xffff - cksum;
    ///   return cksum;
    /// }
    /// ```
    pub e_csum: u16,

    /// Initial IP value, offset 0x14.
    ///
    /// This holds the address to load into the IP register to point to the
    /// initial address (the entry point) within the code segment and, in
    /// that sense, it is absolute.
    pub e_ip: u16,

    /// Initial (relative) CS value, offset 0x16.
    ///
    /// A paragraph is 16 bytes because of memory segmentation in the x86
    /// processor.
    ///
    /// This is the paragraph address of the code segment relative to the
    /// start of the load module.  The segment address of the load module
    /// is added to this to obtain the segment address of the code, which
    /// is then loaded into the CS register before program start.  This
    /// is necessary because the code may span multiple paragraphs and
    /// we need both CS and IP to locate the entry point.
    pub e_cs: u16,

    /// File address of relocation table, offset 0x18.
    ///
    /// This is the zero-based offset into the file of the relocation table.
    /// Because of the structure of the header it should be at least 0x1c so
    /// that it is past the header, but of course you can override that.  If
    /// there are no relocations, it does not matter.
    ///
    /// If there are overlays (this is explained in the documentation for
    /// `e_ovno`) then overlay information often comes before the relocation
    /// table.
    ///
    /// If the value is 0x40, then this may indicate that the file is not a
    /// DOS MZ executable, but don't assume.
    pub e_lfarlc: u16,

    /// Overlay number, offset 0x1a.
    ///
    /// This is the last value in the fixed header.
    ///
    /// If the program does not have an overlay then this is 0x0000.  If the
    /// program has an overlay, then this is the overlay number.
    ///
    /// The entire program might not fit into memory.  In this case it may
    /// actually have additional content in the file on disk that is loaded
    /// over other content in memory (overlay, get it?).
    ///
    /// Okay, but what does this actually mean?  Well, it depends on the
    /// implementation of overlays used.  Programs use overlay managers to
    /// make this work, and the overlay manager is likely part of the
    /// program (linked with it).  The interpretation of this is up to the
    /// program.
    ///
    /// Overlay information is usually contained in the file immediately
    /// following this entry and before the start of the relocation table.
    pub e_ovno: u16,
}

impl MZHeader {
    /// Get the default MZ header.
    pub fn new() -> Self {
        Self {
            e_magic: [b'M', b'Z'],
            e_cblp: 0,
            e_cp: 1,
            e_crlc: 0,
            e_cparhdr: 4,
            e_minalloc: 0,
            e_maxalloc: 0,
            e_ss: 0,
            e_sp: 0,
            e_csum: 0,
            e_ip: 0,
            e_cs: 0,
            e_lfarlc: 0,
            e_ovno: 0,
        }
    }
}

impl From<Blob> for MZHeader {
    fn from(blob: Blob) -> Self {
        let mut header = MZHeader::new();
        let mut bi = BlobIterator::new(blob);
        header.e_magic = [bi.next_u8().unwrap_or(0), bi.next_u8().unwrap_or(0)];
        header.e_cblp = bi.next_u16().unwrap_or(0);
        header.e_cp = bi.next_u16().unwrap_or(0);
        header.e_crlc = bi.next_u16().unwrap_or(0);
        header.e_cparhdr = bi.next_u16().unwrap_or(0);
        header.e_minalloc = bi.next_u16().unwrap_or(0);
        header.e_maxalloc = bi.next_u16().unwrap_or(0);
        header.e_ss = bi.next_u16().unwrap_or(0);
        header.e_sp = bi.next_u16().unwrap_or(0);
        header.e_csum = bi.next_u16().unwrap_or(0);
        header.e_ip = bi.next_u16().unwrap_or(0);
        header.e_cs = bi.next_u16().unwrap_or(0);
        header.e_lfarlc = bi.next_u16().unwrap_or(0);
        header.e_ovno = bi.next_u16().unwrap_or(0);
        header
    }
}

impl From<MZHeader> for Blob {
    fn from(header: MZHeader) -> Blob {
        let mut bb = BlobBuilder::new();
        bb.add_u8(header.e_magic[0]);
        bb.add_u8(header.e_magic[1]);
        bb.add_u16(header.e_cblp);
        bb.add_u16(header.e_cp);
        bb.add_u16(header.e_crlc);
        bb.add_u16(header.e_cparhdr);
        bb.add_u16(header.e_minalloc);
        bb.add_u16(header.e_maxalloc);
        bb.add_u16(header.e_ss);
        bb.add_u16(header.e_sp);
        bb.add_u16(header.e_csum);
        bb.add_u16(header.e_ip);
        bb.add_u16(header.e_cs);
        bb.add_u16(header.e_lfarlc);
        bb.add_u16(header.e_ovno);
        bb.finish()
    }
}

/// A single MZ file relocation.
///
/// Specify a relocation by giving an offset into the load image in the
/// file.  That is, a relocation is relative to the start of the load
/// image, not relative to the start of the file.  That is all that is
/// needed here.
///
/// Two items are provided:
///
///   * The `offset` within a segment
///   * The `segment` offset, relative to the load segment
///
/// You can think of these as far pointers to locations in the load image.
///
/// If the program must be relocated in memory, then the relocation table
/// is used.  Otherwise, if the program gets its desired address, the
/// table is not used.
///
/// # Example
///
/// Assume we have a relocation pointing to `0x0010:002e`.  Note that this is a
/// 20-bit offset into the load image, not an address in memory.  This points
/// to position `0x0012e` in the load image.
///
/// Suppose the load image is loaded at address `0x1000:0100`.  The segment address
/// `0x1000` is placed in the segment registers, but the `0x0100` offset needs to
/// be taken into account.  This is where relocations are used, and only used when
/// this is non-zero.
///
/// Given our relocation above, we obtain the word located at position `0x0012e` in
/// the load image, which is memory address `0x1000:0100 + 0x0012e = 0x1000:022e`.
/// We then add the offset `0x0100` to that word and write it back.
///
/// Suppose `0x4fe6` (bytes `0xe6` and `0x4f` because of little endian order) was
/// found at that position.  Then we compute `0x4fe6 + 0x0100 = 0x50e6`
/// and we write the word `0x50e6` at position `0x0012e` (address `0x1000:022e`).
///
#[derive(Debug, Clone, Copy)]
pub struct MZRelocation {
    /// The relocation's in-segment offset.
    pub offset: u16,
    /// The relocation's segment offset.
    pub segment: u16,
}

impl MZRelocation {
    /// Make a new relocation.
    pub fn new(offset: u16, segment: u16) -> Self {
        Self { offset, segment }
    }
}

/// Implement the MZ file structure.
///
/// To use this, create a new instance with [`Self::new()`] and then
/// specify the parts with the methods.  Note that the methods will
/// automatically update the header, so if you want to create a corrupt
/// header, you should modify the header *after* you finish adding
/// everything.
///
/// The checksum can be updated *independently* of the rest of the header,
/// and if you want the checksum to be correct but still be able to
/// mangle the header, then add all data, mangle the header, and then
/// adjust the checksum last with [`MZFile::fix_checksum()`].
///
#[derive(Debug, Clone, Default)]
pub struct MZFile {
    /// The file header.
    header: MZHeader,

    /// Data between the file header and the relocation table,
    /// if any.
    data: Vec<u8>,

    /// The relocation table.
    relocations: Vec<MZRelocation>,

    /// Executable bytes of the program.  This can be anything you wish.
    program: Vec<u8>,
}

impl MZFile {
    /// Create a new, essentially empty MZ file.
    pub fn new() -> Self {
        let mut ret = Self {
            header: MZHeader::new(),
            data: vec![],
            relocations: vec![],
            program: vec![],
        };
        ret.fix_header();
        ret
    }

    /// Fix the header fields based on the content of the file.
    fn fix_header(&mut self) {
        let mut size = mem::size_of::<MZHeader>();
        size += self.data.len() * mem::size_of::<u8>();

        // Set relocation information.
        self.header.e_lfarlc = size as u16;
        self.header.e_crlc = self.relocations.len() as u16;
        size += self.relocations.len() * mem::size_of::<MZRelocation>();

        // Set the header length in paragraphs and make sure it is even.
        self.header.e_cparhdr = ((size + 15) / 16) as u16;
        if self.header.e_cparhdr % 2 == 1 {
            self.header.e_cparhdr += 1;
        }
        size += self.program.len() * mem::size_of::<u8>();

        // Set the last page of the file.
        self.header.e_cp = (size / 512) as u16;
        self.header.e_cblp = (size % 512) as u16;
        if self.header.e_cblp > 0 {
            self.header.e_cp += 1;
        }

        // Fix the checksum.
        self.fix_checksum();
    }

    /// Update the checksum for the file.
    pub fn fix_checksum(&mut self) -> &mut Self {
        self.header.e_csum = 0u16;
        let bytes = Vec::<u8>::from(&*self);
        let mut csum = 0u16;
        for index in (0..bytes.len()).step_by(2) {
            csum += u16::from_le_bytes(bytes[index..index + 2].try_into().unwrap());
        }
        self.header.e_csum = 0xffff - csum;
        self
    }

    /// Get the header from the file.  This returns a copy of the header.
    pub fn get_header(&self) -> MZHeader {
        self.header
    }

    /// Set the header for the file.
    pub fn set_header(&mut self, header: MZHeader) -> &mut Self {
        self.header = header;
        self
    }

    /// Set the minimum extra paragraphs required for the program and the
    /// maximum extra paragraphs desired by the program.
    ///
    /// Note that min and max are not checked to see if they make sense.
    pub fn set_alloc(&mut self, min: u16, max: u16) -> &mut Self {
        self.header.e_minalloc = min;
        self.header.e_maxalloc = max;
        self
    }

    /// Set the initial stack pointer value for the program.  This is
    /// relative to the program segment.  No sanity checking is performed.
    pub fn set_stack(&mut self, offset: u16, segment: u16) -> &mut Self {
        self.header.e_ss = segment;
        self.header.e_sp = offset;
        self
    }

    /// Set the initial program entry point.  No sanity checking is
    /// performed.
    pub fn set_entry(&mut self, offset: u16, segment: u16) -> &mut Self {
        self.header.e_cs = segment;
        self.header.e_ip = offset;
        self
    }

    /// Set the overlay number.
    pub fn set_overlay_number(&mut self, number: u16) -> &mut Self {
        self.header.e_ovno = number;
        self
    }

    /// Set the content of the extra bytes between the header and the
    /// relocation table.  Any prior data is discarded.
    pub fn set_data(&mut self, data: &[u8]) -> &mut Self {
        self.data = data.to_vec();
        self.fix_header();
        self
    }

    /// Get the data element of the file.
    pub fn get_data(&self) -> Vec<u8> {
        self.data.clone()
    }

    /// Add a relocation to the program.
    ///
    /// This just adds the relocation to the sequence of relocations.
    /// It does not check to see, for instance, if the relocation is
    /// actually past the end of the program.
    pub fn add_relocation(&mut self, offset: u16, segment: u16) -> &mut Self {
        self.relocations.push(MZRelocation::new(offset, segment));
        self.fix_header();
        self
    }

    /// Get the relocations, if any, for the program.
    pub fn get_relocations(&self) -> Vec<MZRelocation> {
        self.relocations.clone()
    }

    /// Set the program by providing a "binary blob" consisting of a
    /// sequence of bytes.  Any prior program data is discarded.
    ///
    /// Note that this must be the blob using zero as the image load
    /// base.
    pub fn set_program(&mut self, program: &[u8]) -> &mut Self {
        self.program = program.to_vec();
        self.fix_header();
        self
    }

    /// Get the program data, if any.  This returns the program using
    /// zero as the load image base.  If you need to apply an offset, use
    /// the [`MZFile::get_relocated_program()`] method.
    pub fn get_program(&self) -> Vec<u8> {
        self.program.clone()
    }

    /// Get the program from the file, applying relocations based
    /// on the given load address.
    ///
    /// Note that the offset address is limited to 16 bits.  This works
    /// because the code segment is set elsewhere, and this only updates
    /// references relative to the code segment, so only 16 bits are needed
    /// even though the address space is 20 bits.
    pub fn get_relocated_program(&self, address: u16) -> Vec<u8> {
        let mut data = self.program.clone();

        // Now process the relocation table and apply the relocations.
        for relocation in &self.relocations {
            // Pull apart the relocation.
            let offset = relocation.offset;
            let segment = relocation.segment;
            // Compute the offset into the load image and update the word at
            // that address.
            let imageaddr = ((segment << 4) + offset) as usize;
            let word = data[imageaddr] as u16 + ((data[imageaddr] as u16) << 8) + address;
            // Write that word back to the image.
            data[imageaddr] = (word & 0xff) as u8;
            data[imageaddr] = (word >> 8) as u8;
        }

        // Done.  Return the modified data.
        data
    }
}

impl From<&MZFile> for Vec<u8> {
    /// Package the given MZ file as a sequence of bytes.  This encodes the
    /// content of the file, attempting to do the correct thing even when
    /// the header is invalid.  Note in particular that the file layout is
    /// determined by the actual data, and not by the header fields.
    fn from(file: &MZFile) -> Vec<u8> {
        // Compute the size of the vector we need.  This is obtained
        // by computing it, not from the header.  This allows writing
        // an improper file.
        let size: usize = mem::size_of::<MZHeader>()
            + file.data.len() * mem::size_of::<u8>()
            + file.relocations.len() * mem::size_of::<MZRelocation>()
            + file.program.len() * mem::size_of::<MZRelocation>();
        let mut bytes = vec![0; size];

        // Magic string.
        bytes[0..2].clone_from_slice(&file.header.e_magic);

        // Get ready to process the header.
        let mut here = 2;
        let mut next_u16 = |value: u16| {
            let thing = u16::to_le_bytes(value);
            bytes[here..here + 2].clone_from_slice(&thing);
            here += 2;
        };

        // Process the header.
        next_u16(file.header.e_cblp);
        next_u16(file.header.e_cp);
        next_u16(file.header.e_crlc);
        next_u16(file.header.e_cparhdr);
        next_u16(file.header.e_minalloc);
        next_u16(file.header.e_maxalloc);
        next_u16(file.header.e_ss);
        next_u16(file.header.e_sp);
        next_u16(file.header.e_csum);
        next_u16(file.header.e_ip);
        next_u16(file.header.e_cs);
        next_u16(file.header.e_lfarlc);
        next_u16(file.header.e_ovno);

        // Process the data, if any.
        if !file.data.is_empty() {
            bytes[here..here + file.data.len()].clone_from_slice(&file.data);
        }
        here += file.data.len();

        // Process the relocations, if any.
        for reloc in &file.relocations {
            // Convert the relocation into bytes and store it.
            bytes[here..here + 2].clone_from_slice(&u16::to_le_bytes(reloc.offset));
            bytes[here + 2..here + 4].clone_from_slice(&u16::to_le_bytes(reloc.segment));
            here += 4;
        }

        // Process the program, if any.
        if !file.program.is_empty() {
            bytes[here..here + file.program.len()].clone_from_slice(&file.program);
        }
        here += file.program.len();

        // Sanity check.
        assert_eq!(here, size);

        // Complete.
        bytes
    }
}

impl From<&[u8]> for MZFile {
    /// Given a slice of bytes, attempt to parse an MZ file from it.  This
    /// does a best effort parse, and tries to use whatever data is present.
    /// It will not fail, even if you give it garbage or an empty slice.
    fn from(blob: &[u8]) -> Self {
        // Get the length of the blob.
        let length = blob.len();

        // Magic string.
        let mut file = Self::new();
        if length >= 1 {
            file.header.e_magic[0] = blob[0];
        }
        if length >= 2 {
            file.header.e_magic[1] = blob[1];
        }

        // Get ready to process the header.
        let mut here = 2;
        let mut next_u16 = || {
            let value = if length >= here + 2 {
                u16::from_le_bytes(blob[here..here + 2].try_into().unwrap())
            } else {
                0u16
            };
            here += 2;
            value
        };

        // Process the header.
        file.header.e_cblp = next_u16();
        file.header.e_cp = next_u16();
        file.header.e_crlc = next_u16();
        file.header.e_cparhdr = next_u16();
        file.header.e_minalloc = next_u16();
        file.header.e_maxalloc = next_u16();
        file.header.e_ss = next_u16();
        file.header.e_sp = next_u16();
        file.header.e_csum = next_u16();
        file.header.e_ip = next_u16();
        file.header.e_cs = next_u16();
        file.header.e_lfarlc = next_u16();
        file.header.e_ovno = next_u16();

        // Sanity check.
        assert_eq!(here, 0x1c);

        // Process the data, if any.  The data occupies the bytes between
        // the end of the fixed header at 0x1c and the start of the relocation
        // table, the start of the program, or the end of the file, whichever
        // comes first.
        let start_of_data = here;
        let mut end_of_data = if file.header.e_lfarlc > 0 {
            file.header.e_lfarlc
        } else {
            file.header.e_cparhdr * 16
        } as usize;
        end_of_data = end_of_data.clamp(start_of_data, length);
        if end_of_data > start_of_data {
            // There is at least one byte of data.
            file.set_data(&blob[start_of_data..end_of_data]);
        }
        here = end_of_data;

        // Compute the offset to the start of the program and the end of
        // the program.  Watch for end of file.
        let start_of_program = usize::max(file.header.e_cparhdr as usize * 16, length);

        // Process the relocations, if any.  The relocations occupy the
        // space after the data (if any) and before the program, and they
        // must be specified by the header.  Of course, the header can be
        // corrupt, so this will be a best effort.
        for _ in 0..file.header.e_crlc {
            // Expect to find the offset first, followed by the segment.
            if here + 2 > length {
                break;
            }
            let offset = u16::from_le_bytes(blob[here..here + 2].try_into().unwrap());
            here += 2;
            if here + 2 > length {
                break;
            }
            let segment = u16::from_le_bytes(blob[here..here + 2].try_into().unwrap());
            here += 2;
            file.add_relocation(offset, segment);
        } // Handle all relocations.

        // Process the program, if any.  The program is specified by the
        // header, and extends possibly to the end of the file.
        file.set_program(&blob[start_of_program..length]);

        // Built.
        file
    }
}
