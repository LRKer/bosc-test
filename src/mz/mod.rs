// The Bosc Library
// Copyright (c) 2022 by Stacy Prowell.  All rights reserved.
// https://gitlab.com/sprowell/bosc

//! This module implements the Microsoft MZ file format and all associated pieces.

pub mod mzfile;
