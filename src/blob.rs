// The Bosc Library
// Copyright (c) 2022 by Stacy Prowell.  All rights reserved.
// https://gitlab.com/sprowell/bosc

//! Manipulate "blobs".  These are collections of bytes with no intrinsic
//! metadata or structure.  They are a trivial wrapper for `Vec<u8>`, but
//! there are additional methods implemented for blobs.
//!
//! You can extract various primitive types from the blob, and can write
//! primitive types to the blob.  The blob is expanded as necessary.
//!
//! # Endianness
//!
//! This implementation *assumes* little endianness.  This is because the
//! headers for Windows PE files (and related files) only permit little
//! endian encoding, even on big-endian platforms (like the XBox 360), so far
//! as the authors know.
//!
//! To make sure this code works correctly on platforms that might themselves
//! be big endian, all conversions are done *explicitly* in the code.  This
//! has the downside of preventing the use of generics, but should have the
//! upside of always working as expected.

use super::byte_store::Store;

/// Implement a basic blob structure.
///
/// The methods here don't use generics at present, because we have to
/// explicitly control the endianness.  Perhaps that will be something
/// to change in the future.
///
/// # Examples
///
/// You can convert quickly between a blob and a vector of bytes.  The following
/// demonstrates creating a blob, adding some data (and overwriting data that is
/// present) and then comparing it to a vector of bytes.
///
/// ```
/// # use bosc::blob::Blob;
///
/// // Make a new blob, which starts empty.
/// let mut blob = Blob::new();
/// assert!(blob.is_empty());
///
/// // Add some data to the blob.  Note the blob grows automatically.
/// blob.set_u8(0,0x41);            // Blob is [0x41]
/// blob.set_u8(1,0x42);            // Blob is [0x41, 0x42]
/// blob.set_u16(4, 0x8808);        // Blob is [0x41, 0x42, 0x00, 0x00, 0x08, 0x88]
/// assert_eq!(blob.get_u8(5), Some(0x88u8));
///
/// // You can overwrite data in the blob.
/// blob.set_u32(5, 0x00212223);    // Blob is [0x41, 0x42, 0x00, 0x00, 0x08, 0x23, 0x22, 0x21, 0x00]
/// assert_eq!(blob.len(), 9);
/// println!("value: {:x}", blob.get_u16(4).unwrap());
/// assert_eq!(blob.get_u16(4), Some(0x2308u16));
/// assert_eq!(blob.get_u32(1), Some(0x08000042u32));
/// assert_eq!(blob.get_u64(5), None);
///
/// // You can convert any blob to a vector of bytes, and vice-versa.
/// blob.set_u64(16, 0x1122334455667788);
/// let vec = Vec::<u8>::from(blob);
/// let mask = vec![
///     0x41u8, 0x42u8, 0x00u8, 0x00u8, 0x08u8, 0x23u8, 0x22u8, 0x21u8,
///     0x00u8, 0x00u8, 0x00u8, 0x00u8, 0x00u8, 0x00u8, 0x00u8, 0x00u8,
///     0x88u8, 0x77u8, 0x66u8, 0x55u8, 0x44u8, 0x33u8, 0x22u8, 0x11u8,
/// ];
/// assert_eq!(vec, mask);
/// ```
///
/// While the [Store] class has support for files, it is currently incomplete and does
/// not provide robust error handling.  The following are examples of working with
/// blobs that are (ulitmately) backed by files.
///
/// Create a new file blob.
///
/// ```
/// # use std::fs::remove_file;
/// # use std::fs::File;
/// # use std::io::Read;
/// # use std::io::Write;
/// # use std::io::Result;
/// # use bosc::blob::Blob;
///
/// # fn main() -> Result<()> {
/// let data = vec![ 0x41u8, 0x42u8, 0x43u8, 0x31u8, 0x32u8, 0x33u8, 0x0du8, 0x0au8 ];
/// # {
/// // Create a blob from data, then write the blob to a file.
/// let blobout = Blob::from(data.clone());
/// let mut file = File::create("eraseme.bin")?;
/// file.write(&mut Vec::<u8>::from(blobout))?;
/// file.flush()?;
/// # }
///
/// # {
/// // Now read the data back from the file.
/// let mut file = File::open("eraseme.bin")?;
/// let mut content = vec![0u8; 16384];
/// let n = file.read(&mut content)?;
/// content = content[0..n].to_vec();
/// let blobin = Blob::from(content);
/// assert_eq!(data, Vec::<u8>::from(blobin));
/// # }
/// #
/// # // Delete the file.
/// # remove_file("eraseme.bin");
/// # Ok(())
/// # }
/// ```
#[derive(Clone, Debug)]
pub struct Blob {
    /// The raw data making up the blob.
    pub data: Store,
}

impl Default for Blob {
    fn default() -> Self {
        Self::new()
    }
}

impl From<Vec<u8>> for Blob {
    fn from(data: Vec<u8>) -> Self {
        Self {
            data: Store::MemoryStore(data),
        }
    }
}

impl From<Blob> for Vec<u8> {
    fn from(blob: Blob) -> Vec<u8> {
        blob.data.get_all()
    }
}

impl Blob {
    /// Make an empty blob.
    pub fn new() -> Self {
        Self { data: Store::new() }
    }

    /// Get the length of the blob.
    pub fn len(&self) -> usize {
        self.data.len()
    }

    /// See if the blob is empty.
    pub fn is_empty(&self) -> bool {
        self.data.is_empty()
    }

    /// Obtain a blob iterator for this blob.  The iterator takes ownership of
    /// the blob.
    pub fn into_blob_iterator(self) -> BlobIterator {
        BlobIterator::new(self)
    }

    /// Make a blob with a specific size.  Careful here!  Methods that set
    /// an element can exceed this limit and grow the blob.
    pub fn with_capacity(capacity: usize) -> Self {
        Self {
            data: Store::MemoryStore(Vec::with_capacity(capacity)),
        }
    }

    /// Resize the blob.  If the new size is less than the current size, then
    /// truncate the blob.  If it is greater, then add zero bytes to the blob
    /// to reach the requested size.  If the new size is the same as the old size,
    /// then nothing is done.
    pub fn resize(&mut self, size: usize) {
        self.data.resize(size);
    }

    /// Write the given blob into this blob, starting at the given offset.  This
    /// may resize the blob to fit, and may overwrite existing data.
    pub fn write_into(&mut self, blob: &Blob, offset: usize) {
        self.data.resize(offset);
        let mut data = blob.data.get_all();
        self.data.append(&mut data);
    }

    /// Extract part of this blob to create a new blob.  The returned blob will
    /// always be the requested size, even if there is no matching data in this
    /// blob; any extra bytes are set to zero.
    pub fn extract(&self, offset: usize, length: usize) -> Blob {
        let mut blob = Blob::new();
        if offset < self.data.len() {
            let mut bytes = self.data.get_bytes(offset, length).unwrap();
            blob.data.append(&mut bytes);
        }
        blob.data.resize(length);
        blob
    }

    /// Append the contents of the given blob to the end of this blob.  The given
    /// blob is consumed.
    pub fn append(&mut self, blob: Blob) {
        self.data.append(&mut blob.data.get_all());
    }

    /// Write a byte to the blob.  If the given offset does not exist,
    /// then the blob is expanded (with zeros) until it does.
    pub fn set_u8(&mut self, offset: usize, value: u8) {
        self.data.put_bytes(offset, &value.to_le_bytes());
    }

    /// Get a byte from the blob.
    pub fn get_u8(&self, offset: usize) -> Option<u8> {
        // Not using get directly, because we want to force the byte to
        // be officially cloned.
        self.data.get_bytes(offset, 1).map(|value| value[0])
    }

    /// Write a word to the blob.  If the word at the given offset does not
    /// exist, then the blob is expanded (with zeros) until it does.
    pub fn set_u16(&mut self, offset: usize, value: u16) {
        self.data.put_bytes(offset, &value.to_le_bytes());
    }

    /// Get a word from the blob.
    pub fn get_u16(&self, offset: usize) -> Option<u16> {
        self.data
            .get_bytes(offset, 2)
            .map(|value| u16::from_le_bytes(value[0..2].try_into().unwrap()))
    }

    /// Write a dword to the blob.  If the dword at the given offset does not
    /// exist, then the blob is expanded (with zeros) until it does.
    pub fn set_u32(&mut self, offset: usize, value: u32) {
        self.data.put_bytes(offset, &value.to_le_bytes());
    }

    /// Get a dword from the blob.
    pub fn get_u32(&self, offset: usize) -> Option<u32> {
        self.data
            .get_bytes(offset, 4)
            .map(|value| u32::from_le_bytes(value[0..4].try_into().unwrap()))
    }

    /// Write a qword to the blob.  If the qword at the given offset does not
    /// exist, then the blob is expanded (with zeros) until it does.
    pub fn set_u64(&mut self, offset: usize, value: u64) {
        self.data.put_bytes(offset, &value.to_le_bytes());
    }

    /// Get a qword from the blob.
    pub fn get_u64(&self, offset: usize) -> Option<u64> {
        self.data
            .get_bytes(offset, 8)
            .map(|value| u64::from_le_bytes(value[0..8].try_into().unwrap()))
    }
}

/// Provide a simple structure to quickly build a blob from data.  Create the builder
/// and then invoke methods to add data to the blob, sequentially.  When done use
/// [`BlobBuilder::finish`] to return the completed blob.  The `finish` method resets
/// the builder.
///
/// # Example
///
/// The blob builder is intended to make construction of a blob very simple.
///
/// ```
/// # use bosc::blob::Blob;
/// # use bosc::blob::BlobBuilder;
/// // Make a builder and add data.
/// let mut bb = BlobBuilder::new();
/// bb.add_u16(0x2132);
///
/// // Skip some locations and add more data.
/// bb.move_to(8);
/// bb.add_u8(0x21);
/// bb.add_u8(0x22);
/// bb.add_u32(0x88084101);
/// bb.add_u64(0x9988776655443322);
///
/// // Obtain the finished blob and check it.
/// let blob = bb.finish();
/// let vector = Vec::<u8>::from(blob);
/// assert_eq!(vector, vec![
///     0x32u8, 0x21u8, 0x00u8, 0x00u8, 0x00u8, 0x00u8, 0x00u8, 0x00u8,
///     0x21u8, 0x22u8, 0x01u8, 0x41u8, 0x08u8, 0x88u8, 0x22u8, 0x33u8,
///     0x44u8, 0x55u8, 0x66u8, 0x77u8, 0x88u8, 0x99u8,
/// ]);
/// ```
pub struct BlobBuilder {
    /// The blob that we are building.
    blob: Blob,
    /// Index into the blob of the next item to insert.
    index: usize,
}

impl Default for BlobBuilder {
    fn default() -> Self {
        Self::new()
    }
}

impl BlobBuilder {
    /// Make a new blob builder starting with an empty blob and initial
    /// offset of zero.
    pub fn new() -> Self {
        Self {
            blob: Blob::new(),
            index: 0,
        }
    }

    /// Instantiate a new blob builder, given the initial blob and
    /// setting the current offset to the given value.
    pub fn initialize(blob: Blob, offset: usize) -> Self {
        Self {
            blob,
            index: offset,
        }
    }

    /// Move to a specific offset in the blob.  This method
    /// will typically be used only once, at the start.  After
    /// that, items can be inserted using the other methods.
    /// Note that this does not modify the blob, only the offset
    /// of the *next* item to be inserted.
    pub fn move_to(&mut self, offset: usize) {
        self.index = offset;
    }

    /// Add a byte to the next position in the blob.
    pub fn add_u8(&mut self, value: u8) {
        self.blob.set_u8(self.index, value);
        self.index += 1;
    }

    /// Add a word to the next position in the blob.
    pub fn add_u16(&mut self, value: u16) {
        self.blob.set_u16(self.index, value);
        self.index += 2;
    }

    /// Add a dword to the next position in the blob.
    pub fn add_u32(&mut self, value: u32) {
        self.blob.set_u32(self.index, value);
        self.index += 4;
    }

    /// Add a qword to the next position in the blob.
    pub fn add_u64(&mut self, value: u64) {
        self.blob.set_u64(self.index, value);
        self.index += 8;
    }

    /// Skip the given number of bytes in the blob, setting them to zero.
    pub fn skip(&mut self, length: usize) {
        self.blob.resize(self.index + length);
        self.index += length;
    }

    /// Complete and return the blob, and then reset this builder.
    pub fn finish(&mut self) -> Blob {
        let blob = Blob::new();
        let built = std::mem::replace(&mut self.blob, blob);
        self.index = 0;
        built
    }
}

/// Iterate over a blob, extracting items.
///
/// This is not a proper iterator because you have to specify the specific
/// thing to extract each time, and that (at present) isn't done with any
/// sort of generics.
///
/// # Example
///
/// The blob iterator is intended to make is easy to walk over a blob,
/// extracting the different data from it.
///
/// ```
/// # use bosc::blob::Blob;
/// # use bosc::blob::BlobIterator;
/// // Create a blob from a vector of bytes.
/// let vector = vec![
///     0x32u8, 0x21u8, 0x00u8, 0x00u8, 0x00u8, 0x00u8, 0x00u8, 0x00u8,
///     0x21u8, 0x22u8, 0x01u8, 0x41u8, 0x08u8, 0x88u8, 0x22u8, 0x33u8,
///     0x44u8, 0x55u8, 0x66u8, 0x77u8, 0x88u8, 0x99u8,
/// ];
/// let blob = Blob::from(vector);
///
/// // Convert the blob into a blob iterator.
/// let mut bi = blob.into_blob_iterator();
/// assert_eq!(bi.next_u64(), Some(0x0000000000002132u64));
/// assert_eq!(bi.next_u32(), Some(0x41012221u32));
/// assert_eq!(bi.next_u16(), Some(0x8808u16));
/// assert_eq!(bi.next_u8(), Some(0x22u8));
///
/// // If a complete item cannot be obtained, then `None` is returned.
/// assert_eq!(bi.next_u64(), None);
/// ```
pub struct BlobIterator {
    /// The blob.
    blob: Blob,
    /// The current position.
    index: usize,
}

impl BlobIterator {
    /// Make a new blob interator iterating over the content of the given
    /// blob.  Be aware that this iterator owns the blob that you provide.
    pub fn new(blob: Blob) -> Self {
        Self { blob, index: 0 }
    }

    /// Skip some bytes in the blob.
    pub fn skip(&mut self, length: usize) {
        self.index += length;
    }

    /// Return the next byte starting at the current position in the blob.
    pub fn next_u8(&mut self) -> Option<u8> {
        let value = self.blob.get_u8(self.index);
        self.index += 1;
        value
    }

    /// Return the next word starting at the current position in the blob.
    pub fn next_u16(&mut self) -> Option<u16> {
        let value = self.blob.get_u16(self.index);
        self.index += 2;
        value
    }

    /// Return the next dword starting at the current position in the blob.
    pub fn next_u32(&mut self) -> Option<u32> {
        let value = self.blob.get_u32(self.index);
        self.index += 4;
        value
    }

    /// Return the next qword starting at the current position in the blob.
    pub fn next_u64(&mut self) -> Option<u64> {
        let value = self.blob.get_u64(self.index);
        self.index += 8;
        value
    }
}
