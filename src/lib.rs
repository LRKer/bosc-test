// The Bosc Library
// Copyright (c) 2022 by Stacy Prowell.  All rights reserved.
// https://gitlab.com/sprowell/bosc

//! # Bosc Library
//!
//! The Bosc library is an attempt to implement parsers and generators for
//! the MZ, NT, and PE file formats.
//!
//! * [`mz`] MZ File Format
//! * [`pe`] COFF and PE File Formats

// Specify the project's logo.
#![doc(html_logo_url = "../bosc.png", html_favicon_url = "../small-bosc.png")]
// Warnings FAIL the build when the "strict" feature is set.
#![cfg_attr(feature = "strict", deny(warnings))]
// Lints!
#![deny(
    missing_docs,
    missing_copy_implementations,
    trivial_casts,
    trivial_numeric_casts,
    unsafe_code,
    unstable_features,
    unused_import_braces,
    unused_qualifications
)]

pub mod blob;
pub mod byte_store;
pub mod mz;
pub mod ne;
pub mod pe;
