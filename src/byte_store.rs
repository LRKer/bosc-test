// The Bosc Library
// Copyright (c) 2022 by Stacy Prowell.  All rights reserved.
// https://gitlab.com/sprowell/bosc

//! Provide for byte storage in blobs.

use std::cell::RefCell;
use std::fs::File;
use std::io::Read;
use std::io::Result;
use std::io::Seek;
use std::io::SeekFrom;
use std::io::Write;

/// Provide backing store.
#[derive(Debug)]
pub enum Store {
    /// Backing store is in memory.
    MemoryStore(Vec<u8>),
    /// Backing store is in a file.
    FileStore(RefCell<File>),
}

impl Clone for Store {
    fn clone(&self) -> Self {
        match self {
            Self::MemoryStore(mem) => Self::MemoryStore(mem.clone()),
            Self::FileStore(file) => {
                let newfile = file.borrow().try_clone().unwrap();
                Self::FileStore(RefCell::new(newfile))
            }
        }
    }
}

impl Default for Store {
    fn default() -> Self {
        Self::new()
    }
}

/// Interface for a byte storage.
impl Store {
    /// Get the current length of the byte storage, in bytes.
    pub fn len(&self) -> usize {
        match self {
            Self::MemoryStore(mem) => mem.len(),
            Self::FileStore(file) => {
                let meta = file
                    .borrow()
                    .metadata()
                    .expect("Unable to get file metadata.");
                meta.len() as usize
            }
        }
    }

    /// See if the byte storage is empty.
    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    /// Resie the byte storage to the given size.  If the new size is less than
    /// the original size, then the byte storage is truncated.  If it is greater,
    /// then the storage is grown (with zero bytes) to the new size.
    ///
    /// Some backing stores may experience failure.
    pub fn resize(&mut self, newsize: usize) {
        match self {
            Self::MemoryStore(mem) => mem.resize(newsize, 0),
            Self::FileStore(file) => {
                let _ = file.borrow_mut().set_len(newsize as u64);
                let _ = file.borrow_mut().sync_all();
            }
        }
    }

    /// Get bytes from the byte storage.  This copies the bytes out of the
    /// backing store.  If the required length cannot be read, or if the offset
    /// is not present in the file, then `None` is returned.
    ///
    /// Some backing stores require mutability to move an internal pointer on read.
    pub fn get_bytes(&self, offset: usize, length: usize) -> Option<Vec<u8>> {
        match self {
            Self::MemoryStore(mem) => {
                if offset + length > mem.len() {
                    None
                } else {
                    Some(mem[offset..(offset + length)].try_into().unwrap())
                }
            }
            Self::FileStore(file) => {
                let mut buf = Vec::new();
                buf.resize(length, 0u8);
                let _ = file.borrow_mut().seek(SeekFrom::Start(offset as u64));
                match file.borrow_mut().read(&mut buf) {
                    Ok(n) if n == length => Some(buf),
                    _ => None,
                }
            }
        }
    }

    /// Copy the given bytes into the byte storage.  This copies bytes to the
    /// backing store.  If the offset does not exist, then zero bytes are added
    /// until it does, so this can grow the size of the byte storage.
    ///
    /// Some backing stores may experience failure.
    pub fn put_bytes(&mut self, offset: usize, bytes: &[u8]) {
        match self {
            Self::MemoryStore(mem) => {
                mem.resize(offset, 0u8);
                mem.append(&mut bytes.to_owned());
            }
            Self::FileStore(file) => {
                let _ = file.borrow_mut().seek(SeekFrom::Start(offset as u64));
                let _ = file.borrow_mut().write(bytes);
            }
        }
    }

    /// Get all bytes from the byte storage.  This is roughly equivalent to
    /// `store.get_bytes(0, store.len())`, but not returning an option.
    pub fn get_all(&self) -> Vec<u8> {
        match self {
            Self::MemoryStore(mem) => mem.clone(),
            Self::FileStore(file) => {
                let _ = file.borrow_mut().seek(SeekFrom::Start(0u64));
                let mut bytes = Vec::new();
                let _ = file.borrow_mut().read_to_end(&mut bytes);
                bytes
            }
        }
    }

    /// Append the given vector's items to the end of the byte store.
    pub fn append(&mut self, bytes: &mut [u8]) {
        self.put_bytes(self.len(), bytes);
    }

    /// Make a new, empty byte storage.
    pub fn new() -> Self {
        Self::MemoryStore(vec![])
    }

    /// Make a new byte storage wrapping the given vector.
    pub fn from_bytes(data: Vec<u8>) -> Self {
        Self::MemoryStore(data)
    }

    /// Make a new file store for a new, empty file.
    pub fn new_file(path: &str) -> Result<Self> {
        let file = match File::create(path) {
            Ok(file) => file,
            Err(x) => return Err(x),
        };
        Ok(Self::FileStore(RefCell::new(file)))
    }

    /// Make a new file store from an existing file.
    pub fn from_file(path: &str) -> Result<Self> {
        let file = match File::open(path) {
            Ok(file) => file,
            Err(x) => return Err(x),
        };
        Ok(Self::FileStore(RefCell::new(file)))
    }
}
