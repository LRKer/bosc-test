![Bosc](./etc/small-bosc.png "Bosc Pear")

> **WARNING** This library is under active development and has not reached the first release.  It is incomplete, and cannot be used to fully parse or generate PE files at this time.  Contributions are welcome!

# Bosc PE Library

This is the source code for the Bosc library.  This library is intended to support reading, manipulating, and writing PE files (and their relatives).  The goal is to enable modification of the files in arbitrary and sometimes unusual ways.

Bosc uses [Semantic Versioning][].

## About Bosc

Bosc is a library for reading, manipulating, and generating PE files.

Have you found a bug?  Submit it through the [Bosc Issues][] tracker, and be sure to include (1) what happens, (2) what you *expect* to happen, (3) how to *reproduce* the bug, and (4) what *version* of Bosc you are using.

## Online Documentation

For the master branch only:

  * [Bosc Rust API](https://sprowell.gitlab.io/bosc/bosc/)

## Local Documentation

**If you have built the documentation locally**, then this link will take you to it.

  * [Local Bosc Rust API](apidocs.html)

## Building

You need to have Rust 2021 edition installed to compile.  Compilation should be relatively simple.

    $ cargo build

This should build the Bosc library.

If you are on a *nix system, then you might have a look at `etc/build.sh`.  This is a little script that runs the "pre-commit" pipeline.

## API Documentation

If you want documentation run `cargo doc --nodeps`.

After generation the API documentation should be available locally starting at [`target/doc/bosc/index.html`](target/doc/bosc/index.html).

## Security

Please submit sensitive security issues using the "This issue is confidential..." button below the issue description field in the issue tracker.

The code should be checked, periodically, against the known Rust security advisories.  This can be done with `cargo audit`.  You can install that with `cargo install cargo-audit`.

On some flavors of Linux you might find you need to install additional packages first.  Known dependencies (that are apparently not installed automatically or by default) are: `libssl-dev` and `pkg-config`.  Install these with `apt`, `yum`, or whatever package manager you use.

Consider installing [Trivy][].  This is a vulnerability scanner.

## Contribute

See [Contributing][] for information about contributing to this project.

If you are interested in contributing, see the [Roadmap][], the [TechDebt][], and any `TODO` or `BUG` comments in the code for some things that still need to be done.  Others may be working on them, so be sure to check in with the project.

## Copyright

Bosc is Copyright (c) 2022 by Stacy Prowell (sprowell+bosc@gmail.com).  All rights reserved.  See the [Copyright][] and [License][] files for more information.

[Copyright]:        ./COPYRIGHT
[License]:          ./LICENSE
[TechDebt]:         ./TechDebt.md
[Roadmap]:          ./Roadmap.md
[Contributing]:     ./CONTRIBUTING.md
[Bosc Issues]:      https://gitlab.com/sprowell/bosc/-/issues
[Semantic Versioning]: https://semver.org/
[Trivy]:            https://aquasecurity.github.io/trivy/
