#!/usr/bin/env python

# The Bosc Library
# Copyright (c) 2022 by Stacy Prowell.  All rights reserved.
# https://gitlab.com/sprowell/bosc

'''
Find, and update, the comment header of all source files in the Bosc repository.

To avoid errors and false positives, we look for the copyright block in the first
part of the file (see `maximum_lines`) and require it to be a minimum size
(see `minimum_lines`).  We require it to contain a series of marker strings
(see `markers`).  If all criteria are satisfied, it is discarded and replaced with
the given copyright block (see `copyright`) prefixed with the correct single-line
comment inferred from the file.

This means you can start a new file with just the minimum number of lines of comments
that contains all the markers, and it will be expanded magically.  Right now, that
looks as follows.

// Bosc
// Copyright
'''

# Folders to scan.  Note: These are scanned *recursively*, so don't include
# a folder that is underneath another folder.
folders = ["src", "etc", "examples", "docs/src"]

# Extra files to scan, relative to the root folder of the distribution.
files = ["build.rs", "Cargo.toml"]

# Minimum size for a comment block to be considered.  If you change this, update
# the module documentation.
minimum_lines = 2

# Maximum number of lines of the file that must contain the comment block.
maximum_lines = 50

# Marker strings.  All must be found before the file will be processed.  If you
# change this, update the module documentation.
markers = [
    "Bosc",
    "Copyright"
]

# The start of single-line comments.
comment_starts = [ "#", "//" ]

# The correct block to place in the header.  The string YYYY is replaced
# with the current four digit year.
copyright = """\
The Bosc Library
Copyright (c) YYYY by Stacy Prowell.  All rights reserved.
https://gitlab.com/sprowell/bosc
"""

import datetime
import sys

def extract_comment_block(lines, lno, start):
    """Extract the next contiguous comment block and return it."""
    block = []
    # We need to find all the markers.
    marks = set(markers)
    found = set()
    while lines[lno].startswith(start):
        line = lines[lno]
        block.append(lines[lno])
        lno += 1
        for marker in marks:
            if marker in line:
                found.add(marker)
    marks -= found
    # See if the block matches all criteria.  If so, replace it and
    # indicate that we found the block.
    if lno < maximum_lines and len(block) >= minimum_lines and len(marks) == 0:
        # This is the block we have been looking for.  Replace it with the
        # corrected block.  Along the way we update the year.
        newblock = []
        for line in copyright.splitlines(True):
            line = line.replace("YYYY", str(datetime.datetime.now().year))
            line = f"{start} {line}".strip() + "\n"
            newblock.append(line)
        return (newblock, lno, True)
    else:
        # Return the comment block as-is.
        return (block, lno, False)

def process_file(file):
    """Process a single file.

    This looks for the appropriate comment block as described in the module
    help, and replaces the copyright block if found.
    """
    # Open the file and read some lines.
    newlines = []
    found = False
    try:
        with open(file,"rt") as fp:
            # If there were massive files, we could use enumerate to avoid reading
            # the entire file.  But why would there be anything so massive in the
            # source distribution?  Just read all the lines for now.
            lines = fp.readlines()

            # Okay, now we have the lines.  We need to find out if the file has
            # both marker strings in it.  We allow any kind of comment, because
            # why not?
            lno = 0
            while lno < len(lines):
                if not found and lno < maximum_lines:
                    hasstart = ""
                    for start in comment_starts:
                        if lines[lno].startswith(start):
                            hasstart = start
                    if len(hasstart) == 0:
                        # Copy the line to the output lines.
                        newlines.append(lines[lno])
                        lno += 1
                    else:
                        # We may have found a comment block.  Extract it.
                        (block,lno,found) = extract_comment_block(lines, lno, hasstart)

                        # Save this into the new list of lines.
                        newlines.extend(block)
                else:
                    # Copy the line to the output lines.
                    newlines.append(lines[lno])
                    lno += 1

        # The entire file has been processed.  Now write it out if it changed.
        if found:
            with open(file, "wt") as fp:
                for line in newlines:
                    fp.write(line)

    except UnicodeDecodeError:
        print("Not readable text file... ", end="")

    except:
        print("Unexpected error:", sys.exc_info()[0])
    
    # Tell the caller whether the file was changed.
    return found


if __name__ == "__main__":
    print("This will update the comment block in all files under these folders:")
    for folder in folders:
        print(f"  * {folder}")
    print()
    print("The following additional files will be updated, also:")
    for filename in files:
        print(f"  * {filename}")
    print()
    print("No backups are made.  Please commit or stash all work before continuing!")
    print()
    go = input("Proceed? (y/N) ")
    if go.startswith('y'):
        import os
        for filename in files:
            print(f"Processing file {filename}...", end="")
            found = process_file(filename)
            if found:
                print("Updated")
            else:
                print("Ignored")
        for folder in folders:
            for root, dirs, files in os.walk(folder):
                for basename in files:
                    filename = os.path.join(root, basename)
                    print(f"Processing file {filename}... ", end="")
                    found = process_file(filename)
                    if found:
                        print("Updated")
                    else:
                        print("Ignored")
