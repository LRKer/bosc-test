#!/usr/bin/env python3

# This code generates Rust code for the header files.  The generated code
# must be correctly formatted (within reason) and must compile without any
# errors or warnings.

import sys

tables = {}

# Add things you need to import here.
to_import = [
    'crate::blob::Blob',
    'crate::pe::constants::machine_type',
    'crate::pe::constants::magic_number',
    'crate::pe::constants::windows_subsystem',
    'std::time::{SystemTime, UNIX_EPOCH}',
]

# Define headers.  Every header needs is an array of triples, in order.
#
# 0: The size of the header element, in bytes.
# 1: The name of the header element.
# 2: The default value of the header element, expressed as Rust code.
# 3: A documentation comment for the header element.
#
# There should be a single element at the start with zero size that gives
# the name of the header and the documentation string for the header.
tables['COFF_HEADER'] = [
    (0, "COFFHeader", 0, '''
        Implement the COFF header.  In an object file this is the first
        element.  In an image file this comes immediately after the
        signature.'''),
    (2, "machine", 'machine_type::IMAGE_FILE_MACHINE_I386', '''
        This is a number identifying the type of target machine.  See the
        [`crate::pe::constants::machine_type`] trait's associated constants.'''),
    (2, "number_of_sections", 0, '''
        The number of sections.  This provides the size of the section
        table that immediately follows the headers.  The Windows loader
        limits this number to 96, in spite of it being a 16-bit integer.'''),
    (4, "time_date_stamp", '''{
            // Get the current UNIX time/date value.
            SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_secs() as u32
        }''', '''
        The low 32 bites of the UNIX epoch to indicate when the file
        was created.  This will overflow on January 19th, 2038, but by
        then (perhaps) we will be using 64 bits.'''),
    (4, "pointer_to_symbol_table", 0, '''
        The file offset of the COFF symbol table.  If this is zero,
        then no symbol table is present.  The Microsoft documentation
        says this should be zero for an image since COFF debugging
        information is deprecated.'''),
    (4, "number_of_symbols", 0, '''
        The number of symbol entries in the symbol table.  This data
        can be used to locate the string table, which immediately follows
        the symbol table.  The Microsoft documentation says this should
        be zero for an image since COFF debugging information is deprecated.'''),
    (2, "size_of_optional_header", 0, '''
        The size of the optional header, which is required for
        executable files but not for object files.  This value should be
        zero for an object file.  This optional header is defined by
        [`OptionalHeader`].'''),
    (2, "characteristics", 0, '''
        The flags that indicate the attributes of teh file.  For specific
        flag values, see [`crate::pe::constants::characteristics`].'''),
]

tables['OPTIONAL_STANDARD_HEADER'] = [
    (0, "OptionalStandardHeader", 0, '''
        The standard part of the optional header.  These are defined for every
        implementation of COFF (as per Microsoft).'''),
    (2, "magic", 'magic_number::PE32', '''
        Specify the type of image file.  The known types are found in
        [`crate::pe::constants::magic_number`].'''),
    (1, "major_linker_version", 0, '''
        Linker major version number.'''),
    (1, "minor_linker_version", 0, '''
        Linker minor version number.'''),
    (4, "size_of_code", 0, '''
        The size of the code.  This is the sum of the size of all the text sections.'''),
    (4, "size_of_initialized_data", 0, '''
        The size of the initialized data.  This is the size of all the data sections.'''),
    (4, "size_of_uninitialized_data", 0, '''
        Ths size of the uninitialized data.  This is the size of all the bss sections.'''),
    (4, "address_of_entry_point", 0, '''
        The address of the entry point relative to the image base when the
        executable file is loaded into memory. For program images, this is
        the starting address. For device drivers, this is the address of the
        initialization function. An entry point is optional for DLLs. When
        no entry point is present, this field must be zero.'''),
    (4, "base_of_code", 0, '''
        The address that is relative to the image base of the
        beginning-of-code section when it is loaded into memory.'''),
    (4, "base_of_data", 0, '''
        The address that is relative to the image base of the
        beginning-of-data section when it is loaded into memory.
        Note that this item is only included in the file for PE32+
        files.'''),
]

tables['OPTIONAL_WINDOWS_HEADER32'] = [
    (0, "OptionalWindowsHeader32", 0, '''
        Specify additional information required by the linker and loader in Windows.'''),
    (4, "image_base", '0x00400000', '''
        The preferred address of the first byte of image when loaded
        into memory; must be a multiple of 64 K. The default for DLLs
        is `0x10000000`. The default for Windows CE EXEs is `0x00010000`.
        The default for Windows NT, Windows 2000, Windows XP, Windows
        95, Windows 98, and Windows Me is `0x00400000`.'''),
    (4, "section_alignment", 4096, '''
        The alignment (in bytes) of sections when they are loaded into
        memory. It must be greater than or equal to FileAlignment. The
        default is the page size for the architecture.'''),
    (4, "file_alignment", 512, '''
        The alignment factor (in bytes) that is used to align the raw
        data of sections in the image file. The value should be a power
        of 2 between 512 and 64 K, inclusive. The default is 512. If
        the section alignment is less than the architecture's page size,
        then file alignment must match section alignment.'''),
    (2, "major_operating_system_version", 0, '''
        The major version number of the required operating system.'''),
    (2, "minor_operating_system_version", 0, '''
        The minor version number of the required operating system.'''),
    (2, "major_image_version", 0, '''
        The major version number of this image.'''),
    (2, "minor_image_version", 0, '''
        The minor version number of this image.'''),
    (2, "major_subsystem_version", 0, '''
        The major version number of the subsystem.'''),
    (2, "minor_subsystem_version", 0, '''
        The minor version number of the subsystem.'''),
    (4, "win32_version_value", 0, '''
        Reserved; must be zero.'''),
    (4, "size_of_image", 0, '''
        The size (in bytes) of the image, including all headers, as the
        image is loaded in memory. It must be a multiple of the section
        alignment.'''),
    (4, "size_of_headers", 0, '''
        The combined size of an MS-DOS stub, PE header, and section headers
        rounded up to a multiple of the file alignment.'''),
    (4, "check_sum", 0, '''
        The image file checksum. The algorithm for computing the checksum
        is incorporated into `IMAGHELP.DLL`. The following are checked for
        validation at load time: all drivers, any DLL loaded at boot time,
        and any DLL that is loaded into a critical Windows process.'''),
    (2, "subsystem", 'windows_subsystem::IMAGE_SUBSYSTEM_NATIVE_WINDOWS', '''
        The subsystem that is required to run this image. For more information,
        see crate::pe::constants::windows_subsystem`].'''),
    (2, "dll_characteristics", 0, '''
        For more information see [`crate::pe::constants::dll_characteristics`].'''),
    (4, "size_of_stack_reserve", 0, '''
        The size of the stack to reserve. Only the stack commit size
        is committed; the rest is made available one page at a time until
        the reserve size is reached.'''),
    (4, "size_of_stack_commit", 0, '''
        The size of the stack to commit.'''),
    (4, "size_of_heap_reserve", 0, '''
        The size of the local heap space to reserve. Only the heap commit size
        is committed; the rest is made available one page at a time until
        the reserve size is reached.'''),
    (4, "size_of_heap_commit", 0, '''
        The size of the local heap space to commit.'''),
    (4, "loader_flags", 0, '''
        Reserved; must be zero.'''),
    (4, "number_of_rva_and_sizes", 0, '''
        The number of data-directory entries in the remainder of the
        optional header. Each describes a location and size.'''),
]

tables['OPTIONAL_WINDOWS_HEADER32_PLUS'] = [
    (0, "OptionalWindowsHeader32Plus", 0, '''
        Specify additional information required by the linker and loader in
        Windows.'''),
    (8, "image_base", '0x00400000', '''
        The preferred address of the first byte of image when loaded
        into memory; must be a multiple of 64 K. The default for DLLs
        is `0x10000000`. The default for Windows CE EXEs is `0x00010000`.
        The default for Windows NT, Windows 2000, Windows XP, Windows
        95, Windows 98, and Windows Me is `0x00400000`.'''),
    (4, "section_alignment", 4096, '''
        The alignment (in bytes) of sections when they are loaded into
        memory. It must be greater than or equal to FileAlignment. The
        default is the page size for the architecture.'''),
    (4, "file_alignment", 512, '''
        The alignment factor (in bytes) that is used to align the raw
        data of sections in the image file. The value should be a power
        of 2 between 512 and 64 K, inclusive. The default is 512. If
        the section alignment is less than the architecture's page size,
        then file alignment must match section alignment.'''),
    (2, "major_operating_system_version", 0, '''
        The major version number of the required operating system.'''),
    (2, "minor_operating_system_version", 0, '''
        The minor version number of the required operating system.'''),
    (2, "major_image_version", 0, '''
        The major version number of this image.'''),
    (2, "minor_image_version", 0, '''
        The minor version number of this image.'''),
    (2, "major_subsystem_version", 0, '''
        The major version number of the subsystem.'''),
    (2, "minor_subsystem_version", 0, '''
        The minor version number of the subsystem.'''),
    (4, "win32_version_value", 0, '''
        Reserved; must be zero.'''),
    (4, "size_of_image", 0, '''
        The size (in bytes) of the image, including all headers, as the
        image is loaded in memory. It must be a multiple of the section alignment.'''),
    (4, "size_of_headers", 0, '''
        The combined size of an MS-DOS stub, PE header, and section headers
        rounded up to a multiple of the file alignment.'''),
    (4, "check_sum", 0, '''
        The image file checksum. The algorithm for computing the checksum
        is incorporated into `IMAGHELP.DLL`. The following are checked for
        validation at load time: all drivers, any DLL loaded at boot time,
        and any DLL that is loaded into a critical Windows process.'''),
    (2, "subsystem", 'windows_subsystem::IMAGE_SUBSYSTEM_NATIVE_WINDOWS', '''
        The subsystem that is required to run this image. For more information,
        see crate::pe::constants::windows_subsystem`].'''),
    (2, "dll_characteristics", 0, '''
        For more information see [`crate::pe::constants::dll_characteristics`].'''),
    (8, "size_of_stack_reserve", 0, '''
        The size of the stack to reserve. Only the stack commit size
        is committed; the rest is made available one page at a time until
        the reserve size is reached.'''),
    (8, "size_of_stack_commit", 0, '''
        The size of the stack to commit.'''),
    (8, "size_of_heap_reserve", 0, '''
        The size of the local heap space to reserve. Only the heap commit size
        is committed; the rest is made available one page at a time until
        the reserve size is reached.'''),
    (8, "size_of_heap_commit", 0, '''
        The size of the local heap space to commit.'''),
    (4, "loader_flags", 0, '''
        Reserved; must be zero.'''),
    (4, "number_of_rva_and_sizes", 0, '''
        The number of data-directory entries in the remainder of the
        optional header. Each describes a location and size.'''),
]

# Indentation.
INDENT = '    '

def commentify(comment: str, depth: int):
    '''Convert a string into a Rust documentation comment.  Any empty first

    line is suppressed.'''
    first = True
    for line in comment.splitlines():
        line = line.strip()
        if first and line == '':
            continue
        first = False
        print(f'{INDENT*depth}/// {line}')

def get_table(table):
    '''Generate rust class from a table.'''

    name = ''

    # Generate the struct for this header.
    for (width, field, _, comment) in table:
        if width == 0:
            commentify(comment, 0)
            name = field
            print('#[derive(Clone, Debug)]')
            print(f'pub struct {name} {{')
            print(f'{INDENT}blob: Blob,')
            print('}')

    # Did we get the name?
    if name == '':
        print('ERROR: Did not find zero-width field specifying name.')
        sys.exit(1)
    
    # Generate the base getters and setters for the header.
    print(f'''
impl {name} {{
{INDENT}/// Make a new, empty instance.  The resulting header element
{INDENT}/// has no content and has size zero.
{INDENT}pub fn new() -> Self {{ Self {{ blob: Blob::new(), }} }}

{INDENT}/// Make a new instance wrapping the given blob.  Careful!  No
{INDENT}/// size checking is performed on the blob.
{INDENT}pub fn new_from_blob(blob: Blob) -> Self {{ Self {{ blob }} }}''')
    offset = 0
    for (width, field, _, comment) in table:
        if width > 0:
            if width == 1:
                kind = 'u8'
            elif width == 2:
                kind = 'u16'
            elif width == 4:
                kind = 'u32'
            elif width == 8:
                kind = 'u64'
            else:
                print('ERROR: Bad field width')
                sys.exit()
            print('')
            commentify(f'Get the {field} field.\n{comment}', 1)
            print(f'{INDENT}pub fn get_{field}(&self) -> {kind} {{ self.blob.get_{kind}({offset}).unwrap_or(0) }}')
            print('')
            commentify(f'Set the {field} field.\n{comment}', 1)
            print(f'{INDENT}pub fn set_{field}(&mut self, value: {kind}) {{ self.blob.set_{kind}({offset}, value); }}')
    
    # Generate the by-name getters.
    print(f'''
{INDENT}/// Get information about a field by name.  If the given name is
{INDENT}/// present in the header definition, then the returned value will
{INDENT}/// be a pair of the zero-based offset into the header (not the file)
{INDENT}/// and the width of the field in bytes.
{INDENT}/// 
{INDENT}/// If the named field is not present, then the pair (0,0) is returned.
{INDENT}pub fn get_spec(name: &str) -> (usize, usize) {{
{INDENT}{INDENT}match name {{''')
    offset = 0
    for (width, field, _, _) in table:
        if width == 0:
            continue
        print(f'{INDENT}{INDENT}{INDENT}"{field}" => ({offset},{width}),')
        offset += width
    print(f'{INDENT}{INDENT}{INDENT}_ => (0,0),')
    print(f'{INDENT}{INDENT}}}')
    print(f'{INDENT}}}')

    # Add the raw access methods.
    print(f'''
{INDENT}/// Obtain a mutable reference into the underlying blob.
{INDENT}pub fn raw_mut(&mut self) -> &mut Blob {{ &mut self.blob }}

{INDENT}/// Obtain a non-mutable reference into the underlying blob.
{INDENT}pub fn raw(&self) -> &Blob {{ &self.blob }}''')

    # Add the associated constant for the header size.
    print(f'''
{INDENT}/// Total size of the header, in bytes.''')
    print(f'{INDENT}pub const SIZE: usize = {offset};')

    # Complete impl.
    print('}')

    # Add the default.
    print(f'''
impl Default for {name} {{
{INDENT}fn default() -> Self {{
{INDENT}{INDENT}let mut header = Self::new();''')
    for (width, field, defvalue, _) in table:
        if width == 0:
            continue
        print(f'{INDENT}{INDENT}header.set_{field}({defvalue});')
    print(f'''{INDENT}{INDENT}header
{INDENT}}}
}}
    ''')

    # Add from implementations.
    print(f'''
impl From<Blob> for {name} {{
    fn from(blob: Blob) -> Self {{ Self::new_from_blob(blob) }}
}}

impl From<{name}> for Blob {{
    fn from(header: {name}) -> Blob {{ header.blob }}
}}
''')

def main():
    '''Build the Rust code for the header tables.'''

    print('''
// Bosc
// Copyright

//! Define the header formats for COFF and PE files.
''')
    for thing in to_import:
        print(f'use {thing};')
    print('''
// WARNING: The code in this file was generated from tables.  See the etc folder
// in ths project for coff_headers_gen.py.  Please do not make changes to this file,
// but if necessary make changes to the generator file.  Ultimately this might
// all be replaceable with Rust procedural macros.
''')
    for table in tables.values():
        get_table(table)

if __name__ == '__main__':
    main()
