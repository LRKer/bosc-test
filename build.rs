// This is a build script run by the Rust build process.  It is automatically discovered and run by `cargo`.
fn main() {
    // Copy the images to the output when generating documentation
    println!("cargo:rerun-if-changed=etc");
    std::fs::create_dir_all("target/doc").expect("Unable to create documentation folder.");
    std::fs::copy("etc/small-bosc.png", "target/doc/small-bosc.png")
        .expect("Failed to copy crate favicon when building documentation.");
    std::fs::copy("etc/bosc.png", "target/doc/bosc.png")
        .expect("Failed to copy crate logo when building documentation.");
}
