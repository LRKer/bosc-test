// The Bosc Library
// Copyright (c) 2022 by Stacy Prowell.  All rights reserved.
// https://gitlab.com/sprowell/bosc

use bosc::mz::mzfile::MZFile;
use std::fs::File;
use std::io::Write;

/// Create a trivial DOS (MZ) executable with no significant content.
fn main() -> Result<(), std::io::Error> {
    let mz = MZFile::new();
    let bytes = Vec::<u8>::from(&mz);
    let mut file = File::create("trivial_mz.exe")?;
    file.write_all(&bytes)?;
    Ok(())
}
